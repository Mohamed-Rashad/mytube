﻿namespace MyTube
{
    partial class frmLstInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLstInfo));
            this.panelTitle = new System.Windows.Forms.Panel();
            this.comboFormats = new MetroFramework.Controls.MetroComboBox();
            this.lblFormats = new MetroFramework.Controls.MetroLabel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnDownload = new MetroFramework.Controls.MetroButton();
            this.lblVideos = new MetroFramework.Controls.MetroLabel();
            this.treeVideos = new System.Windows.Forms.TreeView();
            this.formatsMenu = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.menuCopyLink = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenIDM = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.metroStyleManager = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroStyleExtender = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.panelSubtitle = new System.Windows.Forms.Panel();
            this.listViewSubtitles = new MyTube.Controls.ListView();
            this.lblSubtitles = new MetroFramework.Controls.MetroLabel();
            this.panelTitle.SuspendLayout();
            this.BottomPanel.SuspendLayout();
            this.formatsMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).BeginInit();
            this.panelSubtitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitle
            // 
            this.panelTitle.Controls.Add(this.comboFormats);
            this.panelTitle.Controls.Add(this.lblFormats);
            this.panelTitle.Controls.Add(this.lblTitle);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(20, 30);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(480, 95);
            this.panelTitle.TabIndex = 27;
            // 
            // comboFormats
            // 
            this.comboFormats.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboFormats.DropDownHeight = 100;
            this.comboFormats.FormattingEnabled = true;
            this.comboFormats.IntegralHeight = false;
            this.comboFormats.ItemHeight = 23;
            this.comboFormats.Location = new System.Drawing.Point(0, 66);
            this.comboFormats.MaxDropDownItems = 5;
            this.comboFormats.Name = "comboFormats";
            this.comboFormats.Size = new System.Drawing.Size(480, 29);
            this.comboFormats.Sorted = true;
            this.comboFormats.TabIndex = 36;
            this.comboFormats.UseSelectable = true;
            this.comboFormats.SelectedIndexChanged += new System.EventHandler(this.comboFormats_SelectedIndexChanged);
            // 
            // lblFormats
            // 
            this.lblFormats.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblFormats.Location = new System.Drawing.Point(0, 30);
            this.lblFormats.Name = "lblFormats";
            this.lblFormats.Size = new System.Drawing.Size(480, 35);
            this.lblFormats.TabIndex = 38;
            this.lblFormats.Text = "Formats";
            this.lblFormats.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(480, 30);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Title";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblTitle.UseStyleColors = true;
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnCancel);
            this.BottomPanel.Controls.Add(this.btnDownload);
            this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomPanel.Location = new System.Drawing.Point(20, 511);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(480, 49);
            this.BottomPanel.TabIndex = 28;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(308, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(389, 13);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 23);
            this.btnDownload.TabIndex = 3;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseSelectable = true;
            this.btnDownload.UseStyleColors = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // lblVideos
            // 
            this.lblVideos.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblVideos.Location = new System.Drawing.Point(20, 237);
            this.lblVideos.Name = "lblVideos";
            this.lblVideos.Size = new System.Drawing.Size(480, 35);
            this.lblVideos.TabIndex = 32;
            this.lblVideos.Text = "Play list videos";
            this.lblVideos.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // treeVideos
            // 
            this.treeVideos.CheckBoxes = true;
            this.treeVideos.ContextMenuStrip = this.formatsMenu;
            this.treeVideos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeVideos.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeVideos.FullRowSelect = true;
            this.treeVideos.Location = new System.Drawing.Point(20, 272);
            this.treeVideos.Name = "treeVideos";
            this.treeVideos.ShowLines = false;
            this.treeVideos.ShowNodeToolTips = true;
            this.treeVideos.Size = new System.Drawing.Size(480, 239);
            this.treeVideos.TabIndex = 34;
            this.treeVideos.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeVideos_AfterCheck);
            // 
            // formatsMenu
            // 
            this.formatsMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.formatsMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.formatsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCopyLink,
            this.menuOpenIDM});
            this.formatsMenu.Name = "formatsMenu";
            this.formatsMenu.Size = new System.Drawing.Size(182, 48);
            // 
            // menuCopyLink
            // 
            this.menuCopyLink.Name = "menuCopyLink";
            this.menuCopyLink.Size = new System.Drawing.Size(181, 22);
            this.menuCopyLink.Text = "Copy Link Location";
            this.menuCopyLink.Click += new System.EventHandler(this.menuCopyLink_Click);
            // 
            // menuOpenIDM
            // 
            this.menuOpenIDM.Name = "menuOpenIDM";
            this.menuOpenIDM.Size = new System.Drawing.Size(181, 22);
            this.menuOpenIDM.Text = "Download With IDM";
            this.menuOpenIDM.Click += new System.EventHandler(this.menuOpenIDM_Click);
            // 
            // ToolTip1
            // 
            this.ToolTip1.Style = MetroFramework.MetroColorStyle.Blue;
            this.ToolTip1.StyleManager = null;
            this.ToolTip1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroStyleManager
            // 
            this.metroStyleManager.Owner = this;
            // 
            // panelSubtitle
            // 
            this.panelSubtitle.Controls.Add(this.listViewSubtitles);
            this.panelSubtitle.Controls.Add(this.lblSubtitles);
            this.panelSubtitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubtitle.Location = new System.Drawing.Point(20, 125);
            this.panelSubtitle.Name = "panelSubtitle";
            this.panelSubtitle.Size = new System.Drawing.Size(480, 112);
            this.panelSubtitle.TabIndex = 35;
            // 
            // listViewSubtitles
            // 
            this.listViewSubtitles.CheckBoxes = true;
            this.listViewSubtitles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSubtitles.Location = new System.Drawing.Point(0, 35);
            this.listViewSubtitles.Name = "listViewSubtitles";
            this.listViewSubtitles.Size = new System.Drawing.Size(480, 77);
            this.listViewSubtitles.TabIndex = 36;
            this.listViewSubtitles.UseCompatibleStateImageBehavior = false;
            this.listViewSubtitles.View = System.Windows.Forms.View.List;
            this.listViewSubtitles.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewSubtitles_ItemChecked);
            // 
            // lblSubtitles
            // 
            this.lblSubtitles.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSubtitles.Location = new System.Drawing.Point(0, 0);
            this.lblSubtitles.Name = "lblSubtitles";
            this.lblSubtitles.Size = new System.Drawing.Size(480, 35);
            this.lblSubtitles.TabIndex = 40;
            this.lblSubtitles.Text = "Subtitles";
            this.lblSubtitles.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // frmLstInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 580);
            this.Controls.Add(this.treeVideos);
            this.Controls.Add(this.lblVideos);
            this.Controls.Add(this.BottomPanel);
            this.Controls.Add(this.panelSubtitle);
            this.Controls.Add(this.panelTitle);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(520, 580);
            this.Name = "frmLstInfo";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.None;
            this.Text = "Title";
            this.Load += new System.EventHandler(this.frmLstInfo_Load);
            this.panelTitle.ResumeLayout(false);
            this.BottomPanel.ResumeLayout(false);
            this.formatsMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).EndInit();
            this.panelSubtitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitle;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private System.Windows.Forms.Panel BottomPanel;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroButton btnDownload;
        private MetroFramework.Controls.MetroLabel lblVideos;
        private System.Windows.Forms.TreeView treeVideos;
        private MetroFramework.Components.MetroToolTip ToolTip1;
        private MetroFramework.Components.MetroStyleManager metroStyleManager;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender;
        private MetroFramework.Controls.MetroContextMenu formatsMenu;
        private System.Windows.Forms.ToolStripMenuItem menuCopyLink;
        private System.Windows.Forms.ToolStripMenuItem menuOpenIDM;
        private MetroFramework.Controls.MetroComboBox comboFormats;
        private MetroFramework.Controls.MetroLabel lblFormats;
        private System.Windows.Forms.Panel panelSubtitle;
        private Controls.ListView listViewSubtitles;
        private MetroFramework.Controls.MetroLabel lblSubtitles;
    }
}