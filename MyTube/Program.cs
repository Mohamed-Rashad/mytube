﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyTube
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.ThreadException += new ThreadExceptionEventHandler(ThreadException);
            AppDomain.CurrentDomain.UnhandledException += 
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;
                createErrorLog(ex);
            }
            catch {}
        }

        private static void ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            createErrorLog(e.Exception);
        }

        private static void createErrorLog(Exception e)
        {
            try
            {
                string reportDir = Path.GetFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)) + @"\Smart PC Soft\MyTube\Reports";
                if (!Directory.Exists(reportDir))
                {
                    Directory.CreateDirectory(reportDir);
                }

                string reportFile = reportDir + @"\Error_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".txt";
                string error = @"Dear user, Please don't histate to contact us." + Environment.NewLine;
                error += @"Support system : <http://support.smartpcsoft.com>" + Environment.NewLine;
                error += @"Support email : <support@smartpcsoft.com>" + Environment.NewLine;

                error += new string('-', 100) + Environment.NewLine;
                error += @"Application name: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString() + Environment.NewLine;
                error += @"Product name: " + Application.ProductName.ToString() + Environment.NewLine;
                error += @"Application version: " + Application.ProductVersion.ToString() + Environment.NewLine;
                error += new string('-', 100) + Environment.NewLine;
                error += e.ToString();

                StreamWriter stream = new StreamWriter(reportFile, false, System.Text.Encoding.UTF8);
                stream.WriteLine(error);
                stream.Close();

                if (MessageBox.Show("An exception has occurred in MyTube." +
                    Environment.NewLine + "Do you want to open the exception details?",
                    "Unkown Error", MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Stop) == DialogResult.OK)
                    Process.Start(reportFile);
            }
            catch { }
        }
    }
}
