﻿using MyTube.Classes;
using MyTube.Classes.YouTube;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace MyTube
{
    public partial class frmLstInfo : MetroFramework.Forms.MetroForm
    {
        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        private static extern int SetWindowTheme(IntPtr hwnd, string pszSubAppName, string pszSubIdList);

        private YouTubePlayList playList;
        private const string extractor = "youtube:playlist";
        private List<Format> displayFormat = new List<Format>();
        private List<string> displaySubtitle = new List<string>();

        #region "Constractors"
        public frmLstInfo()
        {
            InitializeComponent();
            try
            {
                SetWindowTheme(this.treeVideos.Handle, "Explorer", null);
            }
            catch { }   
        }

        public frmLstInfo(YouTubePlayList playList) : this()
        {
            this.playList = playList;

            //set playlist title
            this.Text = playList.title.Trim();
            this.lblTitle.Text = playList.title.Trim();
            ToolTip1.SetToolTip(lblTitle, playList.title.Trim());

            //Fill treeView
            foreach (YouTube video in playList.entries)
            {
                //root (Title)
                TreeNode rootNode = new TreeNode();
                rootNode.Text = video.title.Trim();
                rootNode.Tag = video;
                //sub node (Formats)
                TreeNode formatNode = new TreeNode();
                formatNode.Text = "Formats";
                //sub Nod (Subtitles)
                TreeNode subtitleNode = new TreeNode();
                subtitleNode.Text = "Subtitles";

                //Add formats nodes
                foreach (Format format in video.formats)
                {
                    string _formatWithSize = format.format.Trim() + "  |  " + format.ext.Trim() 
                            + "  |  " + format.fileSizeInMegabytes;

                    string _format = format.format.Trim() + "  |  " + format.ext.Trim();

                    formatNode.Nodes.Add(_formatWithSize).Tag = format;

                    if (!displayFormat.Contains(format))
                    {
                        //Add format to displayFormat List.
                        displayFormat.Add(format);
                    }
                }

                //Add subtitles nodes
                if (video.subtitles.Count > 0)
                {
                    foreach (string subtitle in video.subtitles.Keys)
                    {
                        foreach (SubTitle subtitleInfo in video.subtitles[subtitle])
                        {
                            string _subtitle = subtitle + "  |  " + subtitleInfo.ext;
                            subtitleNode.Nodes.Add(_subtitle).Tag = subtitleInfo.url.Trim();
                            //Add subtitle to displaySubtitle list.
                            if (!displaySubtitle.Contains(_subtitle))
                            {
                                displaySubtitle.Add(_subtitle);
                            }
                        }
                    }
                }

                //Add sub nodes to root
                rootNode.Nodes.Add(formatNode);
                if (subtitleNode.Nodes.Count > 0)
                {
                    rootNode.Nodes.Add(subtitleNode);
                }
                //Add root to treeView
                treeVideos.Nodes.Add(rootNode);
            }
            
            comboFormats.DataSource = displayFormat;
            comboFormats.DisplayMember = "format";
            comboFormats.ValueMember = "format_id";
            if(displaySubtitle.Count > 0)
            {
                foreach (string subtitle in displaySubtitle)
                {
                    listViewSubtitles.Items.Add(subtitle);
                }
            }
            else
            {
                panelSubtitle.Visible = false;
            }
           

        }

        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void menuCopyLink_Click(object sender, EventArgs e)
        {
            if(treeVideos.SelectedNode != null && treeVideos.SelectedNode.Tag != null)
            {
                string url = null;
                Clipboard.Clear();
                if (treeVideos.SelectedNode.Tag.GetType() == typeof(Format))
                {
                    //Set URL to clipboard
                    try
                    {
                        Format format = (Format)treeVideos.SelectedNode.Tag;
                        url = format.url;
                    }
                    catch { }
                }
                else
                {
                    url = treeVideos.SelectedNode.Tag.ToString().Trim();
                }
                Clipboard.SetText(url);
            }
        }

        private void menuOpenIDM_Click(object sender, EventArgs e)
        {

            if (treeVideos.SelectedNode != null && treeVideos.SelectedNode.Tag != null)
            {
                string url = null;
                if (treeVideos.SelectedNode.Tag.GetType() == typeof(Format))
                {
                    try
                    {
                        Format format = (Format)treeVideos.SelectedNode.Tag;
                        url = format.url;
                    }
                    catch { }
                }
                else
                {
                    url = treeVideos.SelectedNode.Tag.ToString().Trim();
                }
                IDM.result result = IDM.OpenIDM(url);
                if (result == IDM.result.NotFound)
                {
                    //IDM Not Installed
                    MessageBox.Show("Can't find Internet Donwload Manager, 'IDM.exe' file",
                        "Can't find Internet Donwload Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if (result == IDM.result.Error)
                {
                    MessageBox.Show("Unknown error has occurred while execute process to open IDM",
                     "Unknown error has occurred", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }         
        }

        private void comboFormats_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboFormats.SelectedItem != null){
                foreach (TreeNode node in treeVideos.Nodes)
                {
                    if(node.Nodes.Count >= 0)
                    {
                        foreach (TreeNode formatNode in node.Nodes[0].Nodes)
                        {
                            if (formatNode.Tag != null)
                            {
                                if (formatNode.Tag.GetType() == typeof(Format))
                                {
                                    Format selectedFormat = (Format)comboFormats.SelectedItem;
                                    Format videoFormat = (Format)formatNode.Tag;
                                    if (videoFormat.format_id == selectedFormat.format_id)
                                    {
                                        formatNode.Checked = true;
                                        formatNode.Parent.Checked = true;
                                        formatNode.Parent.Parent.Checked = true;
                                    }
                                    else
                                    {
                                        formatNode.Checked = false;
                                    }
                                }
                            }
                        }
                        CheckSelectedFiles();
                    }
                }
            }
        }

        //change fore color & checked status based on format selected or not!
        private void CheckSelectedFiles()
        {
            
            foreach (TreeNode root in treeVideos.Nodes)
            {
                int SelectedItms = 0;  
                foreach (TreeNode formatNode in root.Nodes[0].Nodes)
                {
                    if (formatNode.Checked == true)
                    {
                        SelectedItms++;
                    }
                }
                //check if video have subtitle
                if(root.Nodes.Count >1)
                {
                    foreach (TreeNode subtitleNode in root.Nodes[1].Nodes)
                    {
                        if (subtitleNode.Checked == true)
                        {
                            SelectedItms++;
                        }
                    }
                }
                //change fore color & checked status based on hasSelectedItm
                root.ForeColor = (SelectedItms > 0) ? Color.Black : Color.Red;
                root.ToolTipText = (SelectedItms > 0 ) ? SelectedItms.ToString() + 
                    " file(s) selected" : "No file selected!";
            }
        }

        private void treeVideos_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if(e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Level == 1)
                {
                    return;
                }
                if (e.Node.Level == 0)
                {
                    foreach (TreeNode node in e.Node.Nodes)
                    {
                        node.Checked = e.Node.Checked;
                    }
                    return;
                }
                CheckSelectedFiles();
            }   
        }

        private void listViewSubtitles_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            foreach (TreeNode node in treeVideos.Nodes)
            {
                if (node.Nodes.Count >= 2)
                {
                    foreach (TreeNode subtitleNode in node.Nodes[1].Nodes)
                    {
                        if (subtitleNode.Text == e.Item.Text)
                        {
                            subtitleNode.Checked = e.Item.Checked;
                        }
                    }
                }
            }
            CheckSelectedFiles();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {

            foreach (TreeNode node in treeVideos.Nodes)
            {
                //check if video is selected
                if(node.Checked == true)
                {

                    //cast object from itm tag
                    YouTube video = (YouTube)node.Tag;


                    if (node.Nodes[0].Checked == true)
                    {
                        foreach (TreeNode formatNode in node.Nodes[0].Nodes)
                        {
                            //cast object from itm tag
                            Format format = (Format)formatNode.Tag;

                            //check if video format is selected
                            if (formatNode.Checked == true)
                            {
                                Download downloadInfo = new Download(video.id.Trim(), video.title.Trim(), format.format_id.Trim(),
                                        format.url.Trim(), format.ext.Trim(), format.fileSizeInMegabytes,
                                        "N/A", "0.0%", extractor);

                                Download.AddDownloadInfo(downloadInfo);
                            }
                        }
                    }
                    
                   
                    //check if video have subtitles
                    if (node.Nodes.Count > 1)
                    {
                        if(node.Nodes[1].Checked == true)
                        {
                            foreach (TreeNode subtitleNode in node.Nodes[1].Nodes)
                            {
                                //check if subtitle is selected
                                if (subtitleNode.Checked == true)
                                {
                                    string[] subtitleInfo = subtitleNode.Text.Split(new[] { "  |  " }, StringSplitOptions.None);
                                    Download downloadInfo = new Download(video.id.Trim(), video.id.Trim() + "." + subtitleInfo[0].Trim() , subtitleInfo[0].Trim(),
                                            subtitleNode.Tag.ToString(), subtitleInfo[1].Trim(), "N/A",
                                            "N/A", "0.0%", "youtube:subtitle");
                                    Download.AddDownloadInfo(downloadInfo);
                                }
                            }
                        }     
                    }
                    
                }
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmLstInfo_Load(object sender, EventArgs e)
        {
            try
            {
                this.StyleManager = metroStyleManager;
            }
            catch (Exception) { }

        }
    }
}
