﻿namespace MyTube
{
    partial class frmVideoInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVideoInfo));
            this.MainPanel = new System.Windows.Forms.Panel();
            this.lstFormats = new MyTube.Controls.ListView();
            this.colFormat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colExtintion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colFileSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colUrl = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.formatsMenu = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.FormatsCopyLink = new System.Windows.Forms.ToolStripMenuItem();
            this.FormatIDM = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_Formats = new MetroFramework.Controls.MetroLabel();
            this.lbl_Subtitles = new MetroFramework.Controls.MetroLabel();
            this.lstSubtitles = new MyTube.Controls.ListView();
            this.colSubtitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSubtitleExtintion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSubtitleURL = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.subtitleMenu = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.SubtitleCopyLink = new System.Windows.Forms.ToolStripMenuItem();
            this.SubtitleIDM = new System.Windows.Forms.ToolStripMenuItem();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnDownload = new MetroFramework.Controls.MetroButton();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.descriptionPanel = new MetroFramework.Controls.MetroPanel();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.picThumbnail = new System.Windows.Forms.PictureBox();
            this.lblDuration = new MetroFramework.Controls.MetroLabel();
            this.metroStyleExtender = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.metroStyleManager = new MetroFramework.Components.MetroStyleManager(this.components);
            this.ToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.MainPanel.SuspendLayout();
            this.formatsMenu.SuspendLayout();
            this.subtitleMenu.SuspendLayout();
            this.BottomPanel.SuspendLayout();
            this.panelTitle.SuspendLayout();
            this.descriptionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picThumbnail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).BeginInit();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.lstFormats);
            this.MainPanel.Controls.Add(this.lbl_Formats);
            this.MainPanel.Controls.Add(this.lbl_Subtitles);
            this.MainPanel.Controls.Add(this.lstSubtitles);
            this.MainPanel.Controls.Add(this.BottomPanel);
            this.MainPanel.Controls.Add(this.panelTitle);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(20, 30);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(480, 530);
            this.MainPanel.TabIndex = 0;
            // 
            // lstFormats
            // 
            this.metroStyleExtender.SetApplyMetroTheme(this.lstFormats, true);
            this.lstFormats.CheckBoxes = true;
            this.lstFormats.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFormat,
            this.colExtintion,
            this.colFileSize,
            this.colUrl});
            this.lstFormats.ContextMenuStrip = this.formatsMenu;
            this.lstFormats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstFormats.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstFormats.FullRowSelect = true;
            this.lstFormats.Location = new System.Drawing.Point(0, 165);
            this.lstFormats.MultiSelect = false;
            this.lstFormats.Name = "lstFormats";
            this.lstFormats.Size = new System.Drawing.Size(480, 173);
            this.lstFormats.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstFormats.TabIndex = 0;
            this.lstFormats.TileSize = new System.Drawing.Size(400, 120);
            this.lstFormats.UseCompatibleStateImageBehavior = false;
            this.lstFormats.View = System.Windows.Forms.View.Details;
            // 
            // colFormat
            // 
            this.colFormat.Text = "Format";
            this.colFormat.Width = 180;
            // 
            // colExtintion
            // 
            this.colExtintion.Text = "Extintion";
            this.colExtintion.Width = 91;
            // 
            // colFileSize
            // 
            this.colFileSize.Text = "File Size";
            this.colFileSize.Width = 97;
            // 
            // colUrl
            // 
            this.colUrl.Text = "URL";
            this.colUrl.Width = 100;
            // 
            // formatsMenu
            // 
            this.formatsMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.formatsMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.formatsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FormatsCopyLink,
            this.FormatIDM});
            this.formatsMenu.Name = "formatsMenu";
            this.formatsMenu.Size = new System.Drawing.Size(182, 48);
            // 
            // FormatsCopyLink
            // 
            this.FormatsCopyLink.Name = "FormatsCopyLink";
            this.FormatsCopyLink.Size = new System.Drawing.Size(181, 22);
            this.FormatsCopyLink.Text = "Copy Link Location";
            this.FormatsCopyLink.Click += new System.EventHandler(this.MenuCopyLink_Click);
            // 
            // FormatIDM
            // 
            this.FormatIDM.Name = "FormatIDM";
            this.FormatIDM.Size = new System.Drawing.Size(181, 22);
            this.FormatIDM.Text = "Download With IDM";
            this.FormatIDM.Click += new System.EventHandler(this.MenuOpenIDM_Click);
            // 
            // lbl_Formats
            // 
            this.lbl_Formats.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Formats.Location = new System.Drawing.Point(0, 130);
            this.lbl_Formats.Name = "lbl_Formats";
            this.lbl_Formats.Size = new System.Drawing.Size(480, 35);
            this.lbl_Formats.TabIndex = 22;
            this.lbl_Formats.Text = "Formats";
            this.lbl_Formats.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbl_Subtitles
            // 
            this.lbl_Subtitles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl_Subtitles.Location = new System.Drawing.Point(0, 338);
            this.lbl_Subtitles.Name = "lbl_Subtitles";
            this.lbl_Subtitles.Size = new System.Drawing.Size(480, 35);
            this.lbl_Subtitles.TabIndex = 24;
            this.lbl_Subtitles.Text = "Subtitles";
            this.lbl_Subtitles.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lstSubtitles
            // 
            this.metroStyleExtender.SetApplyMetroTheme(this.lstSubtitles, true);
            this.lstSubtitles.CheckBoxes = true;
            this.lstSubtitles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSubtitle,
            this.colSubtitleExtintion,
            this.colSubtitleURL});
            this.lstSubtitles.ContextMenuStrip = this.subtitleMenu;
            this.lstSubtitles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lstSubtitles.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstSubtitles.FullRowSelect = true;
            this.lstSubtitles.Location = new System.Drawing.Point(0, 373);
            this.lstSubtitles.MultiSelect = false;
            this.lstSubtitles.Name = "lstSubtitles";
            this.lstSubtitles.Size = new System.Drawing.Size(480, 108);
            this.lstSubtitles.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstSubtitles.TabIndex = 1;
            this.lstSubtitles.TileSize = new System.Drawing.Size(400, 120);
            this.lstSubtitles.UseCompatibleStateImageBehavior = false;
            this.lstSubtitles.View = System.Windows.Forms.View.Details;
            // 
            // colSubtitle
            // 
            this.colSubtitle.Text = "Language";
            this.colSubtitle.Width = 128;
            // 
            // colSubtitleExtintion
            // 
            this.colSubtitleExtintion.Text = "Extintion";
            this.colSubtitleExtintion.Width = 150;
            // 
            // colSubtitleURL
            // 
            this.colSubtitleURL.Text = "URL";
            this.colSubtitleURL.Width = 188;
            // 
            // subtitleMenu
            // 
            this.subtitleMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.subtitleMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.subtitleMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SubtitleCopyLink,
            this.SubtitleIDM});
            this.subtitleMenu.Name = "formatsMenu";
            this.subtitleMenu.Size = new System.Drawing.Size(182, 48);
            // 
            // SubtitleCopyLink
            // 
            this.SubtitleCopyLink.Name = "SubtitleCopyLink";
            this.SubtitleCopyLink.Size = new System.Drawing.Size(181, 22);
            this.SubtitleCopyLink.Text = "Copy Link Location";
            this.SubtitleCopyLink.Click += new System.EventHandler(this.SubtitleCopyLink_Click);
            // 
            // SubtitleIDM
            // 
            this.SubtitleIDM.Name = "SubtitleIDM";
            this.SubtitleIDM.Size = new System.Drawing.Size(181, 22);
            this.SubtitleIDM.Text = "Download With IDM";
            this.SubtitleIDM.Click += new System.EventHandler(this.SubtitleIDM_Click);
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnCancel);
            this.BottomPanel.Controls.Add(this.btnDownload);
            this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomPanel.Location = new System.Drawing.Point(0, 481);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(480, 49);
            this.BottomPanel.TabIndex = 25;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(308, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(389, 13);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 23);
            this.btnDownload.TabIndex = 3;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseSelectable = true;
            this.btnDownload.UseStyleColors = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // panelTitle
            // 
            this.panelTitle.Controls.Add(this.descriptionPanel);
            this.panelTitle.Controls.Add(this.lblTitle);
            this.panelTitle.Controls.Add(this.picThumbnail);
            this.panelTitle.Controls.Add(this.lblDuration);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(480, 130);
            this.panelTitle.TabIndex = 26;
            // 
            // descriptionPanel
            // 
            this.descriptionPanel.Controls.Add(this.txtDescription);
            this.descriptionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descriptionPanel.HorizontalScrollbarBarColor = true;
            this.descriptionPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.descriptionPanel.HorizontalScrollbarSize = 10;
            this.descriptionPanel.Location = new System.Drawing.Point(170, 30);
            this.descriptionPanel.Name = "descriptionPanel";
            this.descriptionPanel.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.descriptionPanel.Size = new System.Drawing.Size(310, 80);
            this.descriptionPanel.TabIndex = 8;
            this.descriptionPanel.VerticalScrollbarBarColor = true;
            this.descriptionPanel.VerticalScrollbarHighlightOnWheel = false;
            this.descriptionPanel.VerticalScrollbarSize = 10;
            // 
            // txtDescription
            // 
            this.metroStyleExtender.SetApplyMetroTheme(this.txtDescription, true);
            this.txtDescription.BackColor = System.Drawing.Color.White;
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.txtDescription.Location = new System.Drawing.Point(3, 0);
            this.txtDescription.MaxLength = 0;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDescription.Size = new System.Drawing.Size(307, 80);
            this.txtDescription.TabIndex = 4;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblTitle.Location = new System.Drawing.Point(170, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(310, 30);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Title";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblTitle.UseStyleColors = true;
            // 
            // picThumbnail
            // 
            this.picThumbnail.Dock = System.Windows.Forms.DockStyle.Left;
            this.picThumbnail.Location = new System.Drawing.Point(0, 0);
            this.picThumbnail.Name = "picThumbnail";
            this.picThumbnail.Size = new System.Drawing.Size(170, 110);
            this.picThumbnail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picThumbnail.TabIndex = 0;
            this.picThumbnail.TabStop = false;
            // 
            // lblDuration
            // 
            this.lblDuration.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblDuration.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblDuration.Location = new System.Drawing.Point(0, 110);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(480, 20);
            this.lblDuration.TabIndex = 6;
            this.lblDuration.Text = "Duration";
            this.lblDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDuration.UseStyleColors = true;
            // 
            // metroStyleManager
            // 
            this.metroStyleManager.Owner = this;
            // 
            // ToolTip1
            // 
            this.ToolTip1.Style = MetroFramework.MetroColorStyle.Blue;
            this.ToolTip1.StyleManager = null;
            this.ToolTip1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // frmVideoInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 580);
            this.Controls.Add(this.MainPanel);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(520, 580);
            this.Name = "frmVideoInfo";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.None;
            this.Text = "Title";
            this.Load += new System.EventHandler(this.frmVideoInfo_Load);
            this.MainPanel.ResumeLayout(false);
            this.formatsMenu.ResumeLayout(false);
            this.subtitleMenu.ResumeLayout(false);
            this.BottomPanel.ResumeLayout(false);
            this.panelTitle.ResumeLayout(false);
            this.descriptionPanel.ResumeLayout(false);
            this.descriptionPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picThumbnail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.PictureBox picThumbnail;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private Controls.ListView lstFormats;
        private System.Windows.Forms.ColumnHeader colUrl;
        private System.Windows.Forms.ColumnHeader colFormat;
        private System.Windows.Forms.ColumnHeader colExtintion;
        private System.Windows.Forms.ColumnHeader colFileSize;
        private MetroFramework.Controls.MetroLabel lbl_Formats;
        private MetroFramework.Controls.MetroLabel lblDuration;
        private MetroFramework.Controls.MetroLabel lbl_Subtitles;
        private Controls.ListView lstSubtitles;
        private System.Windows.Forms.ColumnHeader colSubtitleURL;
        private System.Windows.Forms.ColumnHeader colSubtitleExtintion;
        private System.Windows.Forms.ColumnHeader colSubtitle;
        private MetroFramework.Controls.MetroContextMenu formatsMenu;
        private System.Windows.Forms.ToolStripMenuItem FormatsCopyLink;
        private System.Windows.Forms.ToolStripMenuItem FormatIDM;
        private MetroFramework.Controls.MetroContextMenu subtitleMenu;
        private System.Windows.Forms.ToolStripMenuItem SubtitleCopyLink;
        private System.Windows.Forms.ToolStripMenuItem SubtitleIDM;
        private System.Windows.Forms.Panel BottomPanel;
        private MetroFramework.Controls.MetroButton btnDownload;
        private MetroFramework.Controls.MetroButton btnCancel;
        private System.Windows.Forms.Panel panelTitle;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender;
        private MetroFramework.Components.MetroStyleManager metroStyleManager;
        private MetroFramework.Controls.MetroPanel descriptionPanel;
        private System.Windows.Forms.TextBox txtDescription;
        private MetroFramework.Components.MetroToolTip ToolTip1;
    }
}