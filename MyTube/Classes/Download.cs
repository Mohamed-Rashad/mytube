﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace MyTube.Classes
{
    public class Download
    {
        public string id { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string format { get; set; }
        public string ext { get; set; }
        public string fileSize { get; set; }
        public string status { get; set; }
        public string progress { get; set; }
        public string extractor { get; set; }

        public Download()
        {

        }

        public Download(string id, string title, string format, string url,
         string ext, string fileSize, string status, string progress, string extractor)
        {
            this.id = id;
            this.title = title;
            this.format = format;
            this.url = url;
            this.ext = ext;
            this.fileSize = fileSize;
            this.status = status;
            this.progress = progress;
            this.extractor = extractor;
        }

        public static void AddDownloadInfo(Download downloadInfo)
        {
            if (!File.Exists("downloads.xml"))
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;

                using (XmlWriter writer = XmlWriter.Create("downloads.xml", settings))
                {
                    writer.WriteStartDocument(); //Start Document
                    writer.WriteStartElement("downloads");//Root

                    writer.WriteStartElement("file");//Start element

                    //ID Node
                    writer.WriteStartElement("id");
                    writer.WriteCData(downloadInfo.id.Trim());
                    writer.WriteEndElement();


                    //Title Node
                    writer.WriteStartElement("title");
                    writer.WriteCData(downloadInfo.title.Trim());
                    writer.WriteEndElement();


                    //file URL Node
                    writer.WriteStartElement("url");
                    writer.WriteCData(downloadInfo.url);
                    writer.WriteEndElement();

                    //Format ID Node
                    writer.WriteStartElement("format");
                    writer.WriteCData(downloadInfo.format);
                    writer.WriteEndElement();

                    //File ext Node
                    writer.WriteStartElement("ext");
                    writer.WriteCData(downloadInfo.ext);
                    writer.WriteEndElement();


                    //File Size Node
                    writer.WriteStartElement("size");
                    writer.WriteCData(downloadInfo.fileSize);
                    writer.WriteEndElement();


                    //Status Node
                    writer.WriteStartElement("status");
                    writer.WriteCData(downloadInfo.status);
                    writer.WriteEndElement();


                    //Progress Node
                    writer.WriteStartElement("progress");
                    writer.WriteCData(downloadInfo.progress);
                    writer.WriteEndElement();

                    //Extractor Node
                    writer.WriteStartElement("extractor");
                    writer.WriteCData(downloadInfo.extractor);
                    writer.WriteEndElement();

                    writer.WriteEndElement();//end element



                    writer.WriteEndElement();//Close root element
                    writer.WriteEndDocument();//End document
                    writer.Close();
                }
            }
            else
            {
                XmlDocument document = new XmlDocument();
                document.Load("downloads.xml"); // Load document

                //filter duplicate files
                bool isDuplicated = false;
                XmlNode dublicatedNode = null;
                foreach (XmlElement element in document.GetElementsByTagName("file"))
                {
                    XmlNodeList id_Node = element.GetElementsByTagName("id");
                    XmlNodeList format_Node = element.GetElementsByTagName("format");
                    if (id_Node.Count > 0 && format_Node.Count > 0)
                    {
                        if (id_Node[0].InnerText.Equals(downloadInfo.id) && 
                            format_Node[0].InnerText.Equals(downloadInfo.format))
                        {
                            //*** Duplicated file deteced! ***
                            isDuplicated = true;
                            dublicatedNode = element;
                            break;
                        }
                    }
                }

                if (isDuplicated)
                    document.GetElementsByTagName("downloads")[0].RemoveChild(dublicatedNode);

                //create elements
                XmlElement fileElement = document.CreateElement("file");//root element
                XmlElement idElement = document.CreateElement("id");
                XmlElement formatElement = document.CreateElement("format");
                XmlElement titleElement = document.CreateElement("title");
                XmlElement urlElement = document.CreateElement("url");
                XmlElement extElement = document.CreateElement("ext");
                XmlElement sizeElement = document.CreateElement("size");
                XmlElement statusElement = document.CreateElement("status");
                XmlElement progressElement = document.CreateElement("progress");
                XmlElement extractorElement = document.CreateElement("extractor");

                //create id node
                XmlNode idNode = document.CreateCDataSection(downloadInfo.id);
                idElement.AppendChild(idNode);


                //create title node
                XmlNode titleNode = document.CreateCDataSection(downloadInfo.title);
                titleElement.AppendChild(titleNode);

                //create URL node
                XmlNode urlNode = document.CreateCDataSection(downloadInfo.url);
                urlElement.AppendChild(urlNode);

                //create format node
                XmlNode formatNode = document.CreateCDataSection(downloadInfo.format);
                formatElement.AppendChild(formatNode);

                //create ext node
                XmlNode extNode = document.CreateCDataSection(downloadInfo.ext);
                extElement.AppendChild(extNode);


                //create size node
                XmlNode sizeNode = document.CreateCDataSection(downloadInfo.fileSize);
                sizeElement.AppendChild(sizeNode);


                //create ext node
                XmlNode statusNode = document.CreateCDataSection(downloadInfo.status);
                statusElement.AppendChild(statusNode);


                //create progress node
                XmlNode progressNode = document.CreateCDataSection(downloadInfo.progress);
                progressElement.AppendChild(progressNode);

                //create extractor node
                XmlNode extractorNode = document.CreateCDataSection(downloadInfo.extractor);
                extractorElement.AppendChild(extractorNode);


                //append child elements to the root element
                fileElement.AppendChild(idElement);
                fileElement.AppendChild(titleElement);
                fileElement.AppendChild(urlElement);
                fileElement.AppendChild(formatElement);
                fileElement.AppendChild(extElement);
                fileElement.AppendChild(sizeElement);
                fileElement.AppendChild(statusElement);
                fileElement.AppendChild(progressElement);
                fileElement.AppendChild(extractorElement);


                document.DocumentElement.AppendChild(fileElement);//add root element to document

                document.Save("downloads.xml"); //Save file

            }
        }

     
        public static void RemoveDownloadInfo(Download downloadInfo)
        {
            if (File.Exists("downloads.xml"))
            {
                XmlDocument document = new XmlDocument();
                document.Load("downloads.xml"); // Load document
                foreach (XmlElement element in document.GetElementsByTagName("file"))
                {
                    XmlNodeList id_Node = element.GetElementsByTagName("id");
                    XmlNodeList format_Node = element.GetElementsByTagName("format");
                    if (id_Node.Count > 0 && format_Node.Count > 0)
                    {
                        if (id_Node[0].InnerText.Equals(downloadInfo.id) && 
                            format_Node[0].InnerText.Equals(downloadInfo.format))
                        {
                            //remove element
                            element.ParentNode.RemoveChild(element);
                            document.Save("downloads.xml"); //Save file
                            return;
                        }
                    }
                }
            }
        }
    }
}
