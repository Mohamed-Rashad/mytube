﻿using Microsoft.Win32;
using System.Diagnostics;

namespace MyTube.Classes
{
   public static class IDM
    {
        public enum result
        {
            NotFound,
            Error,
            success
        }

        public static result OpenIDM(string URL)
        {
            //Get IDM path
            string idmPath = null;
            idmPath = Registry.GetValue(@"HKEY_CURRENT_USER\Software\DownloadManager", "ExePath", null).ToString();
            if (idmPath == null)
            {
                //IDM Not Installed
                return result.NotFound; 
            }
            else
            {
                //Sent URL to IDM
                try
                {
                    Process procExtractor = new Process();
                    procExtractor.StartInfo.FileName = idmPath;
                    procExtractor.StartInfo.Arguments = @"/d " + URL.Trim();
                    procExtractor.StartInfo.UseShellExecute = false;
                    procExtractor.StartInfo.CreateNoWindow = true;
                    procExtractor.Start();
                    return result.success; 
                }
                catch
                { 
                    return result.success;
                }
            }
        }
    }
}
