﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MyTube.Classes.YouTube
{
   public class YouTube
    {

        public string id { get; set; }
        public string title { get; set; }
        public string thumbnail { get; set; }
        public string description { get; set; }
        public int duration { get; set; }
        public string stringDuration
        {
            get
            {
                TimeSpan videoDuration = TimeSpan.FromSeconds(this.duration);
                return videoDuration.ToString(@"hh\:mm\:ss");
            }
        } 
        public List<Format> formats { get; set; }
        public Dictionary<string,List<SubTitle>> subtitles { get; set; }

    }
}
