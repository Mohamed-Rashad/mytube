﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTube.Classes.YouTube
{
   public class YouTubePlayList
    {
        public string id { get; set; }
        public string title { get; set; }
        public List<YouTube> entries { get; set; }

    }
}
