﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTube.Classes.Facebook
{
    public class Facebook
    {
        public string id { get; set; }
        public string title { get; set; }

        public List<Format> formats { get; set; }
    }
}
