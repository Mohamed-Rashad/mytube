﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTube.Classes.Facebook
{
   public class Format
    {
        private long? fileSize;
        private string formatNote;

        public long? filesize
        {
            set
            {
                if(value > 0)
                {
                    this.fileSize = value;
                }
            }

            get
            {
                return this.fileSize;
            }
        }
        public string url { get; set; }
        public string format_note
        {
            get
            {
                if (string.IsNullOrEmpty(formatNote))
                {
                    return "N/A";
                } 
                else
                {
                    return this.formatNote.Trim();
                }
            }

            set
            {
                this.formatNote = value.Trim();
            }
        }
        public string format_id { get; set; }
        public string ext { get; set; }
        public string fileSizeInMegabytes
        {
            get
            {
                if (this.filesize != null && this.filesize > 0)
                {
                    try
                    {
                        return Math.Round(((float)(this.filesize / 1024f) / 1024f),2).ToString() + " MB";
                    }
                    catch
                    {

                        return "Unkown";
                    }
                }
                else
                {
                    return "Unkown";
                }             
            }
        }
    }
}
