﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MetroFramework.Components;
using Vlc.DotNet.Core;
using System.IO;
using MetroFramework.Controls;
using MetroFramework;
using MediaPlayer.Classes;
using System.Runtime.InteropServices;
using MyTube;
using MyTube.Properties;

namespace MediaPlayer
{
    public partial class MediaPlayer 
    {

        #region Property
        private int _downloaded = 0;
        public string title { get; set; }
        public string snapShotLocation { get; set; }
        public Control parent { get; set; }
        public MetroStyleManager metroStyleManager { get; set; }
        public DirectoryInfo vlcLibDirectory { get; set; }

        public int downloaded
        {
            get { return this._downloaded; }

            set
            {
                this._downloaded = value;
                progress.Value = _downloaded;
                if (_downloaded >= 100)
                    progress.Visible = false;
                else
                    progress.Visible = true;
            }

        }
        #endregion

        #region Declarations
        MetroStyleManager styleManager = new MetroStyleManager();
        MetroProgressBar progress = new MetroProgressBar();
        MetroTrackBar trackTime = new MetroTrackBar();
        MetroToolTip toolTip = new MetroToolTip();
        Panel panelControls = new Panel();
        Panel panelVLC = new Panel();
        Panel panelTransparent = new Panel();
        Panel paneltimeControls = new Panel();
        PictureBox picVolume = new PictureBox();
        PictureBox picFullscreen = new PictureBox();
        PictureBox picPlay = new PictureBox();
        Label lblTotalTime = new Label();
        Label lblElapsedTime = new Label();
        Timer timerChkIdle = new Timer();
        VlcMediaPlayer player;
        SolidBrush brush;
        bool hoverOnPlayer = false;
        bool mouseIsHide = false;
        
        

        //Last Input method
        [DllImport("user32.dll")]
        static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
        [StructLayout(LayoutKind.Sequential)]
        struct LASTINPUTINFO
        {
            public static readonly int SizeOf = Marshal.SizeOf(typeof(LASTINPUTINFO));

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 dwTime;
        }
        #endregion

        #region Constractor
        public MediaPlayer()
        {
           
        }
        #endregion

        #region Events

        #region Controls Events
        private void MediaControls_MouseHover(object sender, EventArgs e)
        {
            hoverOnPlayer = true;
        }

        private void MediaControls_MouseLeave(object sender, EventArgs e)
        {
            hoverOnPlayer = false;
        }

        private void PicPlay_Click(object sender, EventArgs e)
        {
            if (player == null)
                return;
            try
            {
                if (player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Playing)
                {
                    player.Pause();
                    //disable timer to check idle
                    timerChkIdle.Enabled = false;
                    picPlay.Image = ImgHelper.ChangeImgColor(Resources.play,brush.Color);
                    picPlay.Tag = 1;
                }
                else
                {
                    player.Play();
                    //enable timer to check idle
                    timerChkIdle.Enabled = true;
                    picPlay.Image = ImgHelper.ChangeImgColor(Resources.pause, brush.Color);
                    picPlay.Tag = 0;
                }
            }
            catch { }
        }

        private void PicSnapshot_Click(object sender, EventArgs e)
        {
            if (player == null)
                return;
            try
            {
                if (string.IsNullOrEmpty(snapShotLocation))
                    snapShotLocation = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

                FileInfo snapShotFile = new FileInfo(snapShotLocation + @"\" +
                    title + @"_" + DateTime.Now.ToString("dMyyhms") + @".jpg");
                player.TakeSnapshot(snapShotFile);
            }
            catch { }
        }

        private void PicStop_Click(object sender, EventArgs e)
        {
            if (player == null)
                return;
            try
            {
                player.Stop();
                if ((int)picPlay.Tag == 0)
                {
                    picPlay.Image = ImgHelper.ChangeImgColor(Resources.play, brush.Color);
                    picPlay.Tag = 1;
                }
                progress.Visible = false;
            }
            catch { }
        }

        private void PicVolume_Click(object sender, EventArgs e)
        {
            if (player == null)
                return;
            try
            {
                if (player.State==Vlc.DotNet.Core.Interops.Signatures.MediaStates.Playing | 
                    player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Paused &&
                    !player.Audio.IsMute)
                {
                    picVolume.Image = ImgHelper.ChangeImgColor(Resources.volume_mute, brush.Color);
                    picVolume.Tag = 1;
                }
                else
                {
                    picVolume.Image = ImgHelper.ChangeImgColor(Resources.volume, brush.Color);
                    picVolume.Tag = 0;
                }
                player.Audio.ToggleMute();
            }
            catch { }
        }

        private void Fullscreen_Toggle(object sender, EventArgs e)
        {
            this.parent.SuspendLayout();

            if (panelVLC.Parent.GetType() == typeof(Form))
            {
                Form frmFullscreen = (Form)panelVLC.Parent;
                frmFullscreen.SuspendLayout();
                panelVLC.Parent = this.parent;
                panelControls.Parent = this.parent;
                progress.Parent = this.parent;
                frmFullscreen.Close();
                frmFullscreen.Dispose();
                picFullscreen.Image = ImgHelper.ChangeImgColor(Resources.fullscreen, brush.Color);
                picFullscreen.Tag = 0;
                toolTip.SetToolTip(picFullscreen, "FullScreen");
            }
            else
            {
                Form frmFullscreen = new Form();
                SetDoubleBuffered(frmFullscreen);
                frmFullscreen.BackColor = Color.Black;
                frmFullscreen.FormBorderStyle = FormBorderStyle.None;
                frmFullscreen.ShowInTaskbar = false;
                frmFullscreen.TopMost = true;
                frmFullscreen.WindowState = FormWindowState.Maximized;
                frmFullscreen.StartPosition = FormStartPosition.CenterScreen;
                frmFullscreen.Text = title;
                frmFullscreen.PreviewKeyDown += FrmFullscreen_PreviewKeyDown;
                panelVLC.Parent = frmFullscreen;
                panelControls.Parent = frmFullscreen;
                progress.Parent = frmFullscreen; 
                frmFullscreen.Show();
                frmFullscreen.Visible = true;
                picFullscreen.Image = ImgHelper.ChangeImgColor(Resources.fullscreen_exit, brush.Color);
                picFullscreen.Tag = 1;
            }

            this.parent.ResumeLayout();
        }

        private void FrmFullscreen_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Escape && panelVLC.Parent.GetType() == typeof(Form))
            {
                Fullscreen_Toggle(null, null);
            }
        }

        private void TrackVolume_ValueChanged(object sender, EventArgs e)
        {
            if (player == null)
                return;
            try
            {
                MetroTrackBar trackVolume = (MetroTrackBar)sender;
                player.Audio.Volume = trackVolume.Value;
            }
            catch { }
        }

        private void TimerChkIdle_Tick(object sender, EventArgs e)
        {
            if (player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Paused ||
                player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Stopped ||
                player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.NothingSpecial)
            {
                if (mouseIsHide)
                {
                    Cursor.Show();
                    mouseIsHide = false;
                    panelControls.Visible = true;
                    timerChkIdle.Enabled = false;
                    return;
                }
            }

            LASTINPUTINFO lastInput = new LASTINPUTINFO();
            lastInput.cbSize = (uint)Marshal.SizeOf(lastInput);
            if (GetLastInputInfo(ref lastInput))
            {
                if (3 <= (Environment.TickCount - lastInput.dwTime) / 1000)
                {
                    if (hoverOnPlayer && !mouseIsHide)
                    {
                        Cursor.Hide();
                        mouseIsHide = true;
                    }
                    panelControls.Visible = false;
                }
                else
                {
                    if (mouseIsHide)
                    {
                        Cursor.Show();
                        mouseIsHide = false;
                    }
                    panelControls.Visible = true;
                }
            }

        }

        private void TrackTime_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.Type != ScrollEventType.ThumbPosition)
                return;
            if (player == null)
                return;

            double newTime = ((double)e.NewValue / trackTime.Maximum) * 100;

            if (newTime >= _downloaded)
            {
                return;
            }

           if (player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Playing |
               player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Paused)
            {
                player.Time = trackTime.Value * 1000;
            }
        }


        #endregion

        #region VLC Player Events
        private void Player_Status(object sender, EventArgs e)
        {
            if (player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Playing)
            {
                //Change Image & toolTip
                picPlay.Image = ImgHelper.ChangeImgColor(Resources.pause, brush.Color);
                picPlay.Invoke((MethodInvoker)(() => toolTip.SetToolTip(picPlay, "Pause")));
                return;
            }
            else if (player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Stopped)
            {
                //reset trackTime
                trackTime.Invoke((MethodInvoker)(() =>
                trackTime.Value = 0
                ));
                //reset elapsed time
                lblElapsedTime.Invoke((MethodInvoker)(() =>
                lblElapsedTime.Text = "--:--:--"
                ));
                //reset total time
                lblTotalTime.Invoke((MethodInvoker)(() =>
                lblTotalTime.Text = "--:--:--"
                ));
            }
            else if (player.State == Vlc.DotNet.Core.Interops.Signatures.MediaStates.Ended)
            {
                try
                {
                    //stop Player
                    System.Threading.Thread thread =
                    new System.Threading.Thread(delegate () { player.Stop(); });
                    thread.Start();
                }
                catch { }

                //reset trackTime
                trackTime.Invoke((MethodInvoker)(() =>
                trackTime.Value = 0
                ));
                //reset elapsed time
                lblElapsedTime.Invoke((MethodInvoker)(() =>
                lblElapsedTime.Text = "--:--:--"
                ));
                //reset total time
                lblTotalTime.Invoke((MethodInvoker)(() =>
                lblTotalTime.Text = "--:--:--"
                ));
            }
            //change image & tooltip
            picPlay.Image = ImgHelper.ChangeImgColor(Resources.play, brush.Color);
            picPlay.Invoke((MethodInvoker)(() => toolTip.SetToolTip(picPlay, "Play")));
         
        }

        private void Player_TimeChanged(object sender, VlcMediaPlayerTimeChangedEventArgs e)
        {
            lblElapsedTime.Invoke((MethodInvoker)(() =>
            lblElapsedTime.Text = TimeSpan.FromSeconds(e.NewTime / 1000).ToString(@"hh\:mm\:ss")
            ));
            trackTime.Invoke((MethodInvoker)(() =>
            trackTime.Value = (int)TimeSpan.FromSeconds(e.NewTime / 1000).TotalSeconds
            ));
        }

        private void Player_LengthChanged(object sender, VlcMediaPlayerLengthChangedEventArgs e)
        {
            lblTotalTime.Invoke((MethodInvoker)(() =>
            lblTotalTime.Text = TimeSpan.FromTicks((long)e.NewLength).ToString(@"hh\:mm\:ss")
            ));
            trackTime.Invoke((MethodInvoker)(() =>
            trackTime.Maximum = (int)TimeSpan.FromTicks((long)e.NewLength).TotalSeconds
            ));
            trackTime.Invoke((MethodInvoker)(() =>
            trackTime.MouseWheelBarPartitions = trackTime.Maximum
           ));
            
        }
        #endregion

        #endregion

        #region  Methods

        /// <summary>
        /// Add Media Player Controls
        /// </summary>
        private void InitializeComponent()
        {

            #region Panels
            //Set Double Buffered property
            SetDoubleBuffered(panelControls);
            SetDoubleBuffered(panelVLC);
            SetDoubleBuffered(panelTransparent);
            SetDoubleBuffered(paneltimeControls);

            //add panelVLC
            this.parent.Controls.Add(panelVLC);
            //add panelControls
            this.parent.Controls.Add(panelControls);
            //add Transparent panel to VLC panel
            panelVLC.Controls.Add(panelTransparent);
            //add timeControls panel to VLC panelControls
            panelControls.Controls.Add(paneltimeControls);

            //set dock style
            panelControls.Dock = DockStyle.Bottom;
            panelVLC.Dock = DockStyle.Fill;
            panelTransparent.Dock = DockStyle.Fill;

            //set panels properties
            panelControls.Height = 60;
            panelControls.Padding = new Padding(0,3,0,0);
            panelControls.BackColor = Color.White;
            panelVLC.RightToLeft = RightToLeft.No;
            panelVLC.BackColor = Color.Black;
            panelTransparent.BackColor = Color.Transparent;

            paneltimeControls.Height = 15;
            paneltimeControls.Dock = DockStyle.Top;


            //set visiblility
            panelControls.Visible = true;
            panelVLC.Visible = true;
            panelTransparent.Visible = true;
            paneltimeControls.Visible = true;


            //bring to front
            panelVLC.BringToFront();
            panelTransparent.BringToFront();

            //set panelTransparent events
            panelTransparent.DoubleClick += Fullscreen_Toggle;
            panelTransparent.MouseHover += MediaControls_MouseHover;
            panelTransparent.MouseLeave += MediaControls_MouseLeave;
            //set panelControls events
            panelControls.MouseHover += MediaControls_MouseHover;
            panelControls.MouseLeave += MediaControls_MouseLeave;
            //set timeControls events
            paneltimeControls.MouseHover += MediaControls_MouseHover;
            paneltimeControls.MouseLeave += MediaControls_MouseLeave;

            #endregion

            #region progress bar
            SetDoubleBuffered(progress);
            progress.StyleManager = styleManager;
            progress.ProgressBarStyle = ProgressBarStyle.Continuous;
            progress.Value = 0;
            progress.Maximum = 100;
            progress.Height = 10;
            progress.Dock = DockStyle.Bottom;
            progress.Visible = true;
            this.parent.Controls.Add(progress);
            #endregion

            #region Player/Puase Picture
            SetDoubleBuffered(picPlay);
            picPlay.Size = new Size(16, 16);
            picPlay.Tag = 0;
            picPlay.SizeMode = PictureBoxSizeMode.Zoom;
            picPlay.Anchor = AnchorStyles.Top |  AnchorStyles.Left;
            picPlay.Location = new Point(20, (panelControls.Height / 2));
            //set image & change color
            picPlay.Image = ImgHelper.ChangeImgColor(Resources.play, brush.Color);
            //set tooltip  
            toolTip.SetToolTip(picPlay, "Play");
            //set PicPlay events
            picPlay.MouseLeave += MediaControls_MouseLeave;
            picPlay.MouseHover += MediaControls_MouseHover;
            picPlay.Click += PicPlay_Click;
            //add PicPlay to the panelControls
            panelControls.Controls.Add(picPlay);
            #endregion

            #region Stop Picture
            PictureBox picStop = new PictureBox();
            SetDoubleBuffered(picStop);
            picStop.Size = new Size(16, 16);
            picStop.SizeMode = PictureBoxSizeMode.Zoom;
            picStop.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            picStop.Location = new Point(52, (panelControls.Height / 2));
            //set image & change color
            picStop.Image = ImgHelper.ChangeImgColor(Resources.stop, brush.Color);
            //set tooltip  
            toolTip.SetToolTip(picStop, "Stop");
            //set PicStop events
            picStop.MouseLeave += MediaControls_MouseLeave;
            picStop.MouseHover += MediaControls_MouseHover;
            picStop.Click += PicStop_Click;
            //add PicStop to the panelControls
            panelControls.Controls.Add(picStop);
            #endregion

            #region Volume Picture
            SetDoubleBuffered(picVolume);
            picVolume.Size = new Size(20, 20);
            picVolume.Tag = 0;
            picVolume.SizeMode = PictureBoxSizeMode.Zoom;
            picVolume.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            picVolume.Location = new Point(82, (panelControls.Height / 2) - 1);
            //set image & change color
            picVolume.Image = ImgHelper.ChangeImgColor(Resources.volume, brush.Color);
            //set tooltip  
            toolTip.SetToolTip(picVolume, "Mute");
            //set picVolume events
            picVolume.MouseLeave += MediaControls_MouseLeave;
            picVolume.MouseHover += MediaControls_MouseHover;
            picVolume.Click += PicVolume_Click;
            //add PicStop to the panelControls
            panelControls.Controls.Add(picVolume);
            #endregion

            #region Volume TrackBar
            MetroTrackBar trackVolume = new MetroTrackBar(0, 200, 100);
            SetDoubleBuffered(trackVolume);
            trackVolume.Size = new Size(80, 10);
            trackVolume.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            trackVolume.Location = new Point(112, (panelControls.Height / 2) + 3);
            trackVolume.UseCustomBackColor = true;
            trackVolume.BackColor = Color.Transparent;
            trackVolume.Theme = styleManager.Theme;
            trackVolume.Style = styleManager.Style;
            //set trackVolume events
            trackVolume.ValueChanged += TrackVolume_ValueChanged;
            trackVolume.PreviewKeyDown += FrmFullscreen_PreviewKeyDown;
            trackVolume.MouseLeave += MediaControls_MouseLeave;
            trackVolume.MouseHover += MediaControls_MouseHover;
            //set tooltip
            toolTip.SetToolTip(trackVolume,"Volume");
            //add trackVolume to the panelControls
            panelControls.Controls.Add(trackVolume);
            #endregion

            #region Time TrackBar
            SetDoubleBuffered(trackTime);
            trackTime.Size = new Size(80, 15);
            trackTime.Location = new Point(0, 0);
            //set trackTime events
            trackTime.MouseLeave += MediaControls_MouseLeave;
            trackTime.MouseHover += MediaControls_MouseHover;
            trackTime.Scroll += TrackTime_Scroll;
            trackTime.PreviewKeyDown += FrmFullscreen_PreviewKeyDown;

            trackTime.Dock = DockStyle.Top;
            trackTime.UseCustomBackColor = true;
            trackTime.BackColor = Color.Transparent;
            trackTime.Theme = styleManager.Theme;
            trackTime.Style = styleManager.Style;
            
            //set trackTime values
            trackTime.Minimum = 0;
            trackTime.Value = 0;
            //add trackVolume to the panelControls
            paneltimeControls.Controls.Add(trackTime);
          
            #endregion

            #region Elapsed Time Label
            SetDoubleBuffered(lblElapsedTime);
            lblElapsedTime.AutoSize = false;
            lblElapsedTime.Size = new Size(60, 15);
            lblElapsedTime.Location = new Point(0, 0);
            lblElapsedTime.TextAlign = ContentAlignment.TopCenter;
            //set lblElapsedTime events
            lblElapsedTime.MouseLeave += MediaControls_MouseLeave;
            lblElapsedTime.MouseHover += MediaControls_MouseHover;
            lblElapsedTime.Dock = DockStyle.Left;
            lblElapsedTime.Text = "--:--";
            paneltimeControls.Controls.Add(lblElapsedTime);
            #endregion

            #region Total Time Label
            SetDoubleBuffered(lblTotalTime);
            lblTotalTime.AutoSize = false;
            lblTotalTime.Size = new Size(60, 15);
            lblTotalTime.Location = new Point(0, 0);
            lblTotalTime.TextAlign = ContentAlignment.TopCenter;
            //set lblTotalTime events
            lblTotalTime.MouseLeave += MediaControls_MouseLeave;
            lblTotalTime.MouseHover += MediaControls_MouseHover;
            lblTotalTime.Dock = DockStyle.Right;
            lblTotalTime.Text = "--:--";
            paneltimeControls.Controls.Add(lblTotalTime);
            #endregion

            #region Fullscreen Picture
            SetDoubleBuffered(picFullscreen);
            picFullscreen.Size = new Size(20, 20);
            picFullscreen.Tag = 0;
            picFullscreen.SizeMode = PictureBoxSizeMode.Zoom;
            picFullscreen.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            picFullscreen.Location = new Point((panelControls.Width - 30),
                (panelControls.Height / 2));
            //set image & change color
            picFullscreen.Image = ImgHelper.ChangeImgColor(Resources.fullscreen, brush.Color);
            //set tooltip  
            toolTip.SetToolTip(picFullscreen,"FullScreen");
            //set picFullscreen events
            picFullscreen.MouseLeave += MediaControls_MouseLeave;
            picFullscreen.MouseHover += MediaControls_MouseHover;
            picFullscreen.Click += Fullscreen_Toggle;
            //add picFullscreen to the panelControls
            panelControls.Controls.Add(picFullscreen);
            #endregion

            #region SnapShot Picture
            PictureBox picSnapshot = new PictureBox();
            SetDoubleBuffered(picSnapshot);
            picSnapshot.Size = new Size(20, 20);
            picSnapshot.Tag = 0;
            picSnapshot.SizeMode = PictureBoxSizeMode.Zoom;
            picSnapshot.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            picSnapshot.Location = new Point((panelControls.Width - 70),
                (panelControls.Height / 2));
            //set image & change color
            picSnapshot.Image = ImgHelper.ChangeImgColor(Resources.camera, brush.Color);
            //set tooltip  
            toolTip.SetToolTip(picSnapshot, "Take SnapShot");
            //set picSnapshot events
            picSnapshot.MouseLeave += MediaControls_MouseLeave;
            picSnapshot.MouseHover += MediaControls_MouseHover;
            picSnapshot.Click += PicSnapshot_Click; ;
            //add picSnapshot to the panelControls
            panelControls.Controls.Add(picSnapshot);
            #endregion

            //set timerIdle event & interval
            timerChkIdle.Tick += TimerChkIdle_Tick;
            timerChkIdle.Interval = 100;

            //Refresh Panels
            panelVLC.Refresh();
            panelControls.Refresh();
            panelTransparent.Refresh();
            paneltimeControls.Refresh();

        }

        /// <summary>
        /// Set Double Buffered property to the control
        /// </summary>
        /// <param name="control">Target control</param>
        private void SetDoubleBuffered(Control control)
        {
            try
            {
                control.GetType().InvokeMember("DoubleBuffered",
                    System.Reflection.BindingFlags.SetProperty |
                    System.Reflection.BindingFlags.Instance |
                    System.Reflection.BindingFlags.NonPublic, null, control, new object[] { true });
            }
            catch {}
        }

        #endregion

        #region Public Methods

        public void Init()
        {
            //set style properties
            if (metroStyleManager != null)
            {
                styleManager = metroStyleManager;
                switch (styleManager.Style)
                {
                    case MetroColorStyle.Black:
                        brush = MetroBrushes.Black;
                        break;
                    case MetroColorStyle.White:
                        brush = MetroBrushes.White;
                        break;
                    case MetroColorStyle.Silver:
                        brush = MetroBrushes.Silver;
                        break;
                    case MetroColorStyle.Blue:
                        brush = MetroBrushes.Blue;
                        break;
                    case MetroColorStyle.Green:
                        brush = MetroBrushes.Green;
                        break;
                    case MetroColorStyle.Lime:
                        brush = MetroBrushes.Lime;
                        break;
                    case MetroColorStyle.Teal:
                        brush = MetroBrushes.Teal;
                        break;
                    case MetroColorStyle.Orange:
                        brush = MetroBrushes.Orange;
                        break;
                    case MetroColorStyle.Brown:
                        brush = MetroBrushes.Brown;
                        break;
                    case MetroColorStyle.Pink:
                        brush = MetroBrushes.Pink;
                        break;
                    case MetroColorStyle.Magenta:
                        brush = MetroBrushes.Magenta;
                        break;
                    case MetroColorStyle.Purple:
                        brush = MetroBrushes.Purple;
                        break;
                    case MetroColorStyle.Red:
                        brush = MetroBrushes.Red;
                        break;
                    case MetroColorStyle.Yellow:
                        brush = MetroBrushes.Yellow;
                        break;
                    case MetroColorStyle.Custom:
                        brush = MetroBrushes.Custom;
                        break;
                    default:
                        brush = MetroBrushes.Blue;
                        break;
                }
            }
            else
            {
                styleManager.Style = MetroColorStyle.Blue;
                styleManager.Theme = MetroThemeStyle.Light;
                brush = MetroBrushes.Blue;
            }
            toolTip.StyleManager = styleManager;

            InitializeComponent();

            player = new VlcMediaPlayer(vlcLibDirectory);
            //set panel handler to VLC player
            player.VideoHostControlHandle = panelVLC.Handle;

            //set player events
            player.Playing += Player_Status;
            player.Stopped += Player_Status;
            player.Paused += Player_Status;
            player.EndReached += Player_Status;

            player.LengthChanged += Player_LengthChanged;
            player.TimeChanged += Player_TimeChanged;
        }

        /// <summary>
        /// Set media file
        /// </summary>
        /// <param name="mediaFile">Media file</param>
        /// <param name="mediaOptions">Media options</param>
        public void SetMedia(FileInfo mediaFile, string[] mediaOptions)
        {
            if (mediaOptions != null)
                player.SetMedia(mediaFile, mediaOptions);
            else
                player.SetMedia(mediaFile);
        }

        public void Play()
        {
            player.Play();
            timerChkIdle.Enabled = true;
        }

        public void Stop()
        {
            player.Stop();
            timerChkIdle.Enabled = false;
        }

        public void pause()
        {
            player.Pause();
            timerChkIdle.Enabled = false;
        }
        #endregion
    }
}
