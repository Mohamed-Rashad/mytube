﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTube.Classes.Twitter
{
    public class Twitter
    {
        private int _duration;

        public string id { get; set; }
        public string title { get; set; }
        public string thumbnail { get; set; }
        public string description { get; set; }

        public float duration
        {
            get
            {
                try
                {
                    return (int)this._duration;
                }
                catch 
                {

                    return 0;
                }
                
            }

            set
            {
                try
                {
                    this._duration = (int)value;
                }
                catch
                {

                    this._duration = 0;
                }
               
            }
        }

        public string stringDuration
        {
            get
            {
                TimeSpan videoDuration = TimeSpan.FromSeconds(this.duration);
                return videoDuration.ToString(@"hh\:mm\:ss");
            }
        }

        public List<Format> formats { get; set; }

    }
}
