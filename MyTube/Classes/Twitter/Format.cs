﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTube.Classes.Twitter
{
   public class Format
    {
        public string url { get; set; }
        public string ext { get; set; }
        public string format { get; set; }
        public string format_id { get; set; }
    }
}
