﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MediaPlayer.Classes
{
    internal static class ImgHelper
    {
        internal static Image ChangeImgColor(Bitmap source,Color color)
        {
            try
            {
                byte red, green, blue = 0;
                for (int x = 0; x < source.Width -1 ; x++)
                {
                    for (int y = 0; y < source.Height -1 ; y++)
                    {
                        red = source.GetPixel(x, y).R;
                        green = source.GetPixel(x, y).G;
                        blue = source.GetPixel(x, y).B;
                        if(red > 0 | green > 0 | blue > 0)
                        {
                            source.SetPixel(x, y, Color.FromArgb(source.GetPixel(x, y).A, color.R, color.G, color.B));
                        }
                    }
                }
                return source;
            }
            catch (Exception)
            {
                return source;
            }
        }
    }
}
