﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTube.Classes.SoundCloud
{
   public class Format
    {
        private string _format;
        public string url { get; set; }
        public string format
        {
            get
            {
                if (string.IsNullOrEmpty(_format))
                {
                    return "N/A";
                }
                else
                {
                    return this._format.Trim();
                }
            }

            set
            {
                this._format = value.Trim();
            }
        }
        public string format_id { get; set; }
        public string ext { get; set; }
          
    }
}
