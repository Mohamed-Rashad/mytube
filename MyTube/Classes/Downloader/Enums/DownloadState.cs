﻿using System;
using System.Text;

namespace MyTube.Classes.Downloader.Enums
{
    /// <summary>
    /// Download states
    /// </summary>
    public enum DownloadState
    {
        /// <summary>
        /// Download is started
        /// </summary>
        Started, 
        /// <summary>
        /// Download is going on
        /// </summary>
        Downloading, 
        /// <summary>
        /// Download is completed
        /// </summary>
        Completed, 
        /// <summary>
        /// Download is cancelled
        /// </summary>
        Cancelled,
        /// <summary>
        /// An error occured while downloading
        /// </summary>
        Error
    }
}
