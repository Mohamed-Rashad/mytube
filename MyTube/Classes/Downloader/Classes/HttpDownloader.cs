﻿using MyTube.Classes.Downloader.Enums;
using MyTube.Classes.Downloader.Interfaces;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace MyTube.Classes.Downloader.Classes
{
    /// <summary>
    /// ProgressChanged event handler
    /// </summary>
    /// <param name="sender">Sender object</param>
    /// <param name="e">Event arguments</param>
    public delegate void ProgressChangedEventHandler(object sender, DownloadProgressChangedEventArgs e);
    /// <summary>
    /// Contains method to help downloading
    /// </summary>
    public class HttpDownloader : IDownloader
    {
        #region Variables
        /// <summary>
        /// Occurs when the download process is completed
        /// </summary>
        public event EventHandler DownloadCompleted;
        /// <summary>
        /// Occurs when the download process Occurs Error
        /// </summary>
        public event EventHandler DownloadError;
        /// <summary>
        /// Occurs when the download process is cancelled
        /// </summary>
        public event EventHandler DownloadCancelled;
        /// <summary>
        /// Occurs when the download progress is changed
        /// </summary>
        public event ProgressChangedEventHandler DownloadProgressChanged;

        HttpWebRequest req;
        HttpWebResponse resp;
        Stream str;
        FileStream file;
        Stopwatch stpWatch;
        AsyncOperation oprtor;
        int progress, speed, speedBytes;
        long contentLength, bytesReceived;
        string fileURL, destPath;
        DownloadState state;
        #endregion

        #region Properties
        public Download Id { get; set; }
        /// <summary>
        /// Gets content size of the file
        /// </summary>
        public long ContentSize
        {
            get { return contentLength; }
        }
        /// <summary>
        /// Gets the total bytes count received
        /// </summary>
        public long BytesReceived
        {
            get { return bytesReceived; }
        }
        /// <summary>
        /// Gets the current download speed in bytes
        /// </summary>
        public int SpeedInBytes
        {
            get { return speed; }
        }
        /// <summary>
        /// Gets the current download progress over 100
        /// </summary>
        public int Progress
        {
            get { return progress; }
            private set
            {
                if(value != progress)
                {
                    oprtor.Post(new SendOrPostCallback(delegate
                    {
                        if (DownloadProgressChanged != null)
                            DownloadProgressChanged(this, new DownloadProgressChangedEventArgs(progress, speed));
                    }), null);
                }
                progress = value;
            }
        }
        /// <summary>
        /// Get the source URL that will be downloaded when the download process is started
        /// </summary>
        public string FileURL
        {
            get
            {
                return fileURL;
            }
        }
        /// <summary>
        /// Gets the destination path that the file will be saved when the download process is completed
        /// </summary>
        public string DestPath
        {
            get
            {
                return destPath;
            }
        }
        /// <summary>
        /// Gets the value that reports the state of the download process
        /// </summary>
        public DownloadState State
        {
            get
            {
                return state;
            }
            private set
            {
                state = value;
                if (state == DownloadState.Completed && DownloadCompleted != null)
                    oprtor.Post(new SendOrPostCallback(delegate
                    {
                        if (DownloadCompleted != null)
                            DownloadCompleted(this, EventArgs.Empty);
                    }), null);
                else if (state == DownloadState.Cancelled && DownloadCancelled != null)
                    oprtor.Post(new SendOrPostCallback(delegate
                    {
                        if (DownloadCancelled != null)
                            DownloadCancelled(this, EventArgs.Empty);
                    }), null);
                else if (state == DownloadState.Error && DownloadError != null)
                    oprtor.Post(new SendOrPostCallback(delegate
                    {
                        if (DownloadError != null)
                            DownloadError(this, EventArgs.Empty);
                    }), null);
            }
        }
        #endregion

        #region Constructor, Destructor, Download Procedure
        /// <summary>
        /// Creates an instance of the HttpDownloader class
        /// </summary>
        /// <param name="url">Url source string</param>
        /// <param name="destPath">Target file path</param>
        public HttpDownloader(string url, string destPath)
        {
            this.Reset();
            fileURL = url;
            this.destPath = destPath;
            oprtor = AsyncOperationManager.CreateOperation(null);
        }
        /// <summary>
        /// Destructor of the class object
        /// </summary>
        ~HttpDownloader()
        {
            this.Cancel();
        }
        void Download(long offset)
        {

            #region Send Request, Get Response
            try
            {
                req = WebRequest.Create(this.FileURL) as HttpWebRequest;
                req.AddRange(offset);
                req.AllowAutoRedirect = true;
                resp = req.GetResponse() as HttpWebResponse;
                str = resp.GetResponseStream();
                if (resp.ContentLength == offset)
                {
                    state = DownloadState.Completed;
                    this.State = state;
                    return;
                }
                contentLength = offset + resp.ContentLength;
                bytesReceived = offset;
            }
            catch (Exception)
            {
                state = DownloadState.Error;
                this.State = state;
                return;
            }
            #endregion

            if (File.Exists(destPath))
            {
                file = new FileStream(destPath, FileMode.Append, FileAccess.Write,FileShare.ReadWrite);
            }
            else
            {
                file = new FileStream(destPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            }
               

            int bytesRead = 0;
            speedBytes = 0;
            byte[] buffer = new byte[4096];
            stpWatch.Start();
       

            #region Get the data to the buffer, write it to the file
            while ((bytesRead = str.Read(buffer, 0, buffer.Length)) > 0)
            {
                if (state == DownloadState.Cancelled) break;
                state = DownloadState.Downloading;
                file.Write(buffer, 0, bytesRead);
                file.Flush();
                bytesReceived += bytesRead;
                speedBytes += bytesRead;
                this.Progress = (int)(bytesReceived * 100.0 / contentLength);
                speed = (int)(speedBytes / 1.0 / stpWatch.Elapsed.TotalSeconds);
            }
            #endregion

            stpWatch.Reset();
            CloseResources();
            Thread.Sleep(100);
            if (state == DownloadState.Downloading)
            {
                state = DownloadState.Completed;
                this.State = state;
            }
        }

        #endregion

        #region Start, Stop
        /// <summary>
        /// Starts the download async
        /// </summary>
        public void StartAsync()
        {
            if (state != DownloadState.Started & state != DownloadState.Completed & state != DownloadState.Cancelled)
                return;

            state = DownloadState.Started;
         
            if (File.Exists(destPath) == true)
            {
                Task.Factory.StartNew(() =>
                {
                    Download(new FileInfo(destPath).Length);
                });
            }
            else
            {
                Task.Factory.StartNew(() =>
                {
                    Download(0);
                });
            }
        }

        /// <summary>
        /// Cancels the download and deletes the uncompleted file which is saved to destination
        /// </summary>
        public void Cancel()
        {
            if (state == DownloadState.Completed | state == DownloadState.Cancelled | state == DownloadState.Error) return;
            state = DownloadState.Cancelled;
            this.State = state;
            Thread.Sleep(100);
            CloseResources();
        }
        #endregion

        #region Helper Methods
        void Reset()
        {
            progress = 0;
            bytesReceived = 0;
            speed = 0;
            stpWatch = new Stopwatch();
        }
        void CloseResources()
        {
            if (resp != null)
                resp.Close();
            if (file != null)
                file.Close();
            if (str != null)
                str.Close();
        }
        #endregion
    }
    
}
