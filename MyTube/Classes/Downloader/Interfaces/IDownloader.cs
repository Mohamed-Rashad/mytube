﻿using MyTube.Classes.Downloader.Classes;
using MyTube.Classes.Downloader.Enums;
using System;

namespace MyTube.Classes.Downloader.Interfaces
{
    interface IDownloader
    {
        event EventHandler DownloadCancelled;
        event EventHandler DownloadCompleted;
        event EventHandler DownloadError;
        event ProgressChangedEventHandler DownloadProgressChanged;

        long ContentSize { get; }
        long BytesReceived { get; }
        int Progress { get; }
        int SpeedInBytes { get; }
        string FileURL { get; }
        string DestPath { get; }
        DownloadState State { get; }
        void StartAsync();
        void Cancel();
    }
}
