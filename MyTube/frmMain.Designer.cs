﻿namespace MyTube
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.metroStyleManager = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.lstViewImgs = new System.Windows.Forms.ImageList(this.components);
            this.tabDownloads = new MetroFramework.Controls.MetroTabPage();
            this.lstView = new MyTube.Controls.ListView();
            this.colTitle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProgress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTransfer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mainMenu = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.menuDownload = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWatch = new System.Windows.Forms.ToolStripMenuItem();
            this.Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStop = new System.Windows.Forms.ToolStripMenuItem();
            this.Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.progrssBar = new MetroFramework.Controls.MetroProgressBar();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.txtUrl = new MetroFramework.Controls.MetroTextBox();
            this.tabControl = new MetroFramework.Controls.MetroTabControl();
            this.tabWatch = new MetroFramework.Controls.MetroTabPage();
            this.tabAbout = new MetroFramework.Controls.MetroTabPage();
            this.lblCopyrights = new MetroFramework.Controls.MetroLabel();
            this.lblVersion = new MetroFramework.Controls.MetroLabel();
            this.lnkLicense = new MetroFramework.Controls.MetroLink();
            this.lnkSupport = new MetroFramework.Controls.MetroLink();
            this.lblSlogan = new MetroFramework.Controls.MetroLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.metroStyleExtender = new MetroFramework.Components.MetroStyleExtender(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).BeginInit();
            this.tabDownloads.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabAbout.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroStyleManager
            // 
            this.metroStyleManager.Owner = this;
            // 
            // metroToolTip1
            // 
            this.metroToolTip1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroToolTip1.StyleManager = null;
            this.metroToolTip1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // lstViewImgs
            // 
            this.lstViewImgs.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("lstViewImgs.ImageStream")));
            this.lstViewImgs.TransparentColor = System.Drawing.Color.Transparent;
            this.lstViewImgs.Images.SetKeyName(0, "youtube.png");
            this.lstViewImgs.Images.SetKeyName(1, "caption.png");
            this.lstViewImgs.Images.SetKeyName(2, "soundcolud.png");
            this.lstViewImgs.Images.SetKeyName(3, "facebook.png");
            this.lstViewImgs.Images.SetKeyName(4, "twitter.png");
            // 
            // tabDownloads
            // 
            this.tabDownloads.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.tabDownloads.Controls.Add(this.lstView);
            this.tabDownloads.Controls.Add(this.progrssBar);
            this.tabDownloads.Controls.Add(this.metroPanel1);
            this.tabDownloads.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.tabDownloads.HorizontalScrollbarBarColor = true;
            this.tabDownloads.HorizontalScrollbarHighlightOnWheel = false;
            this.tabDownloads.HorizontalScrollbarSize = 10;
            this.tabDownloads.Location = new System.Drawing.Point(4, 38);
            this.tabDownloads.Name = "tabDownloads";
            this.tabDownloads.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.tabDownloads.Size = new System.Drawing.Size(752, 338);
            this.tabDownloads.TabIndex = 0;
            this.tabDownloads.Text = "Downloads";
            this.tabDownloads.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tabDownloads.VerticalScrollbarBarColor = true;
            this.tabDownloads.VerticalScrollbarHighlightOnWheel = false;
            this.tabDownloads.VerticalScrollbarSize = 10;
            // 
            // lstView
            // 
            this.metroStyleExtender.SetApplyMetroTheme(this.lstView, true);
            this.lstView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTitle,
            this.colSize,
            this.colStatus,
            this.colProgress,
            this.colTransfer});
            this.lstView.ContextMenuStrip = this.mainMenu;
            this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstView.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstView.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lstView.FullRowSelect = true;
            this.lstView.LabelWrap = false;
            this.lstView.Location = new System.Drawing.Point(0, 40);
            this.lstView.Name = "lstView";
            this.lstView.ShowItemToolTips = true;
            this.lstView.Size = new System.Drawing.Size(752, 288);
            this.lstView.SmallImageList = this.lstViewImgs;
            this.lstView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstView.TabIndex = 1;
            this.lstView.TileSize = new System.Drawing.Size(400, 120);
            this.lstView.UseCompatibleStateImageBehavior = false;
            this.lstView.View = System.Windows.Forms.View.Details;
            this.lstView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lstView_ItemDrag);
            // 
            // colTitle
            // 
            this.colTitle.Text = "Title";
            this.colTitle.Width = 396;
            // 
            // colSize
            // 
            this.colSize.Text = "File Size";
            this.colSize.Width = 80;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.Width = 100;
            // 
            // colProgress
            // 
            this.colProgress.Text = "Progress";
            // 
            // colTransfer
            // 
            this.colTransfer.Text = "Transfer Rate";
            this.colTransfer.Width = 115;
            // 
            // mainMenu
            // 
            this.mainMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mainMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuDownload,
            this.menuWatch,
            this.Separator1,
            this.menuStop,
            this.Separator2,
            this.menuDelete});
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(129, 104);
            // 
            // menuDownload
            // 
            this.menuDownload.Name = "menuDownload";
            this.menuDownload.Size = new System.Drawing.Size(128, 22);
            this.menuDownload.Text = "Download";
            this.menuDownload.Click += new System.EventHandler(this.menuDownload_Click);
            // 
            // menuWatch
            // 
            this.menuWatch.Name = "menuWatch";
            this.menuWatch.Size = new System.Drawing.Size(128, 22);
            this.menuWatch.Text = "Watch";
            this.menuWatch.Click += new System.EventHandler(this.menuWatch_Click);
            // 
            // Separator1
            // 
            this.Separator1.Name = "Separator1";
            this.Separator1.Size = new System.Drawing.Size(125, 6);
            // 
            // menuStop
            // 
            this.menuStop.Name = "menuStop";
            this.menuStop.Size = new System.Drawing.Size(128, 22);
            this.menuStop.Text = "Stop";
            this.menuStop.Click += new System.EventHandler(this.menuStop_Click);
            // 
            // Separator2
            // 
            this.Separator2.Name = "Separator2";
            this.Separator2.Size = new System.Drawing.Size(125, 6);
            // 
            // menuDelete
            // 
            this.menuDelete.Name = "menuDelete";
            this.menuDelete.Size = new System.Drawing.Size(128, 22);
            this.menuDelete.Text = "Delete";
            this.menuDelete.Click += new System.EventHandler(this.menuDelete_Click);
            // 
            // progrssBar
            // 
            this.progrssBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progrssBar.Location = new System.Drawing.Point(0, 328);
            this.progrssBar.Name = "progrssBar";
            this.progrssBar.ProgressBarStyle = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progrssBar.Size = new System.Drawing.Size(752, 10);
            this.progrssBar.TabIndex = 5;
            this.progrssBar.Theme = MetroFramework.MetroThemeStyle.Light;
            this.progrssBar.Visible = false;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.txtUrl);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 5);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(752, 35);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // txtUrl
            // 
            this.txtUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.txtUrl.CustomButton.Image = global::MyTube.Properties.Resources.download_black;
            this.txtUrl.CustomButton.Location = new System.Drawing.Point(725, 1);
            this.txtUrl.CustomButton.Name = "";
            this.txtUrl.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUrl.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUrl.CustomButton.TabIndex = 1;
            this.txtUrl.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUrl.CustomButton.UseSelectable = true;
            this.txtUrl.FontWeight = MetroFramework.MetroTextBoxWeight.Light;
            this.txtUrl.Lines = new string[0];
            this.txtUrl.Location = new System.Drawing.Point(3, 5);
            this.txtUrl.MaxLength = 32767;
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.PasswordChar = '\0';
            this.txtUrl.PromptText = "Enter URL...";
            this.txtUrl.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUrl.SelectedText = "";
            this.txtUrl.SelectionLength = 0;
            this.txtUrl.SelectionStart = 0;
            this.txtUrl.ShortcutsEnabled = true;
            this.txtUrl.ShowButton = true;
            this.txtUrl.Size = new System.Drawing.Size(747, 23);
            this.txtUrl.TabIndex = 0;
            this.txtUrl.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUrl.UseSelectable = true;
            this.txtUrl.WaterMark = "Enter URL...";
            this.txtUrl.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUrl.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtUrl.ButtonClick += new MetroFramework.Controls.MetroTextBox.ButClick(this.txtUrl_ButtonClick);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabDownloads);
            this.tabControl.Controls.Add(this.tabWatch);
            this.tabControl.Controls.Add(this.tabAbout);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HotTrack = true;
            this.tabControl.ItemSize = new System.Drawing.Size(150, 34);
            this.tabControl.Location = new System.Drawing.Point(20, 50);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(760, 380);
            this.tabControl.TabIndex = 3;
            this.tabControl.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tabControl.UseSelectable = true;
            // 
            // tabWatch
            // 
            this.tabWatch.HorizontalScrollbarBarColor = true;
            this.tabWatch.HorizontalScrollbarHighlightOnWheel = false;
            this.tabWatch.HorizontalScrollbarSize = 10;
            this.tabWatch.Location = new System.Drawing.Point(4, 38);
            this.tabWatch.Name = "tabWatch";
            this.tabWatch.Size = new System.Drawing.Size(752, 338);
            this.tabWatch.TabIndex = 1;
            this.tabWatch.Text = "Watch";
            this.tabWatch.VerticalScrollbarBarColor = true;
            this.tabWatch.VerticalScrollbarHighlightOnWheel = false;
            this.tabWatch.VerticalScrollbarSize = 10;
            // 
            // tabAbout
            // 
            this.tabAbout.Controls.Add(this.lblCopyrights);
            this.tabAbout.Controls.Add(this.lblVersion);
            this.tabAbout.Controls.Add(this.lnkLicense);
            this.tabAbout.Controls.Add(this.lnkSupport);
            this.tabAbout.Controls.Add(this.lblSlogan);
            this.tabAbout.Controls.Add(this.label1);
            this.tabAbout.HorizontalScrollbarBarColor = true;
            this.tabAbout.HorizontalScrollbarHighlightOnWheel = false;
            this.tabAbout.HorizontalScrollbarSize = 10;
            this.tabAbout.Location = new System.Drawing.Point(4, 38);
            this.tabAbout.Name = "tabAbout";
            this.tabAbout.Size = new System.Drawing.Size(752, 338);
            this.tabAbout.TabIndex = 3;
            this.tabAbout.Text = "About";
            this.tabAbout.VerticalScrollbarBarColor = true;
            this.tabAbout.VerticalScrollbarHighlightOnWheel = false;
            this.tabAbout.VerticalScrollbarSize = 10;
            // 
            // lblCopyrights
            // 
            this.lblCopyrights.AutoSize = true;
            this.lblCopyrights.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblCopyrights.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblCopyrights.Location = new System.Drawing.Point(285, 308);
            this.lblCopyrights.Name = "lblCopyrights";
            this.lblCopyrights.Size = new System.Drawing.Size(183, 15);
            this.lblCopyrights.TabIndex = 8;
            this.lblCopyrights.Text = "Copyright  2017  © Smart PC Soft";
            this.lblCopyrights.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(320, 164);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(112, 19);
            this.lblVersion.TabIndex = 7;
            this.lblVersion.Text = "Version 4.0.0 beta";
            // 
            // lnkLicense
            // 
            this.lnkLicense.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkLicense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lnkLicense.Location = new System.Drawing.Point(595, 308);
            this.lnkLicense.Name = "lnkLicense";
            this.lnkLicense.Size = new System.Drawing.Size(154, 19);
            this.lnkLicense.TabIndex = 5;
            this.lnkLicense.Text = "General Public License V3.";
            this.lnkLicense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lnkLicense.UseSelectable = true;
            this.lnkLicense.Click += new System.EventHandler(this.lnkLicense_Click);
            // 
            // lnkSupport
            // 
            this.lnkSupport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkSupport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lnkSupport.Location = new System.Drawing.Point(3, 308);
            this.lnkSupport.Name = "lnkSupport";
            this.lnkSupport.Size = new System.Drawing.Size(137, 19);
            this.lnkSupport.TabIndex = 4;
            this.lnkSupport.Text = "Support && Feedback";
            this.lnkSupport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lnkSupport.UseSelectable = true;
            this.lnkSupport.Click += new System.EventHandler(this.lnkSupport_Click);
            // 
            // lblSlogan
            // 
            this.lblSlogan.AutoSize = true;
            this.lblSlogan.Location = new System.Drawing.Point(269, 140);
            this.lblSlogan.Name = "lblSlogan";
            this.lblSlogan.Size = new System.Drawing.Size(214, 19);
            this.lblSlogan.TabIndex = 3;
            this.lblSlogan.Text = "Powerful video download manager";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(236, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(280, 86);
            this.label1.TabIndex = 10;
            this.label1.Text = "MyTube";
            // 
            // metroStyleExtender
            // 
            this.metroStyleExtender.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BackImagePadding = new System.Windows.Forms.Padding(10, 12, 0, 0);
            this.BackMaxSize = 25;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 450);
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(20, 50, 20, 20);
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.Text = "MyTube";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).EndInit();
            this.tabDownloads.ResumeLayout(false);
            this.mainMenu.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabAbout.ResumeLayout(false);
            this.tabAbout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Components.MetroStyleManager metroStyleManager;
        private MetroFramework.Components.MetroToolTip metroToolTip1;
        private System.Windows.Forms.ImageList lstViewImgs;
        private MetroFramework.Controls.MetroTabControl tabControl;
        private MetroFramework.Controls.MetroTabPage tabDownloads;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTextBox txtUrl;
        private System.Windows.Forms.ColumnHeader colTitle;
        private System.Windows.Forms.ColumnHeader colSize;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.ColumnHeader colProgress;
        private System.Windows.Forms.ColumnHeader colTransfer;
        private MetroFramework.Controls.MetroProgressBar progrssBar;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender;
        private Controls.ListView lstView;
        private MetroFramework.Controls.MetroContextMenu mainMenu;
        private System.Windows.Forms.ToolStripMenuItem menuDownload;
        private System.Windows.Forms.ToolStripMenuItem menuDelete;
        private System.Windows.Forms.ToolStripMenuItem menuStop;
        private System.Windows.Forms.ToolStripMenuItem menuWatch;
        private System.Windows.Forms.ToolStripSeparator Separator1;
        private System.Windows.Forms.ToolStripSeparator Separator2;
        private MetroFramework.Controls.MetroTabPage tabWatch;
        private MetroFramework.Controls.MetroTabPage tabAbout;
        private MetroFramework.Controls.MetroLink lnkLicense;
        private MetroFramework.Controls.MetroLink lnkSupport;
        private MetroFramework.Controls.MetroLabel lblSlogan;
        private MetroFramework.Controls.MetroLabel lblVersion;
        private MetroFramework.Controls.MetroLabel lblCopyrights;
        private System.Windows.Forms.Label label1;
    }
}

