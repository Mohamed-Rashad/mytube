﻿using Microsoft.Win32;
using MyTube.Classes;
using MyTube.Classes.Facebook;
using MyTube.Classes.SoundCloud;
using MyTube.Classes.Twitter;
using MyTube.Classes.YouTube;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace MyTube
{
    public partial class frmVideoInfo : MetroFramework.Forms.MetroForm
    {
        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        private static extern int SetWindowTheme(IntPtr hwnd, string pszSubAppName, string pszSubIdList);

        private YouTube video;
        private SoundCloud soundCloud;
        private Facebook facebook;
        private Twitter twitter;
        private string extractor = "";

        #region "Constractors"
        public frmVideoInfo()
        {
            InitializeComponent();
            try
            {
                SetWindowTheme(this.lstFormats.Handle, "Explorer", null);
                SetWindowTheme(this.lstSubtitles.Handle, "Explorer", null);
            }
            catch { }
        }

        public frmVideoInfo(YouTube video) : this()
        {
            extractor = "youtube";
            this.video = video;
            this.Text = video.title.Trim();
            lblTitle.Text = video.title;
            if (!string.IsNullOrEmpty(video.thumbnail))
            {
                picThumbnail.Load(video.thumbnail.Trim());
            }
            else
            {
                picThumbnail.Visible = false;
            }
            txtDescription.Text = video.description.Trim();
            lblDuration.Text = video.stringDuration;
            foreach (Classes.YouTube.Format format in video.formats)
            {
                ListViewItem lstViewItm = new ListViewItem();
                lstViewItm.Text = format.format;
                lstViewItm.Tag = format.format_id;
                lstViewItm.SubItems.Add(format.ext);
                lstViewItm.SubItems.Add(format.fileSizeInMegabytes);
                lstViewItm.SubItems.Add(format.url);
                lstFormats.Items.Add(lstViewItm);
            }

            if (video.subtitles.Count > 0)
            {
                foreach (string subtitle in video.subtitles.Keys)
                {                  
                    foreach (SubTitle subtitleInfo in video.subtitles[subtitle])
                    {
                        ListViewItem lstViewItm = new ListViewItem();
                        lstViewItm.Text = subtitle;
                        lstViewItm.SubItems.Add(subtitleInfo.ext);
                        lstViewItm.SubItems.Add(subtitleInfo.url);
                        lstSubtitles.Items.Add(lstViewItm);
                    }
                }
            }
            else
            {
                lstSubtitles.Visible = false;
                lbl_Subtitles.Visible = false;
            }

        }

        public frmVideoInfo(SoundCloud soundCloud) : this()
        {
            extractor = "soundcloud";
            lstSubtitles.Visible = false;
            lbl_Subtitles.Visible = false;
            this.soundCloud = soundCloud;
            this.Text = soundCloud.title.Trim();
            if (!string.IsNullOrEmpty(soundCloud.thumbnail))
            {
                picThumbnail.Load(soundCloud.thumbnail.Trim());
            }
            else
            {
                picThumbnail.Visible = false;
            }
            lblTitle.Text = soundCloud.title;
            txtDescription.Text = soundCloud.description.Trim();
            lblDuration.Text = soundCloud.stringDuration;
            foreach (Classes.SoundCloud.Format format in soundCloud.formats)
            {
                ListViewItem lstViewItm = new ListViewItem();
                lstViewItm.Text = format.format;
                lstViewItm.Tag = format.format_id;
                lstViewItm.SubItems.Add(format.ext);
                lstViewItm.SubItems.Add("N/A");
                lstViewItm.SubItems.Add(format.url);
                lstFormats.Items.Add(lstViewItm);
            }

        }

        public frmVideoInfo(Facebook facebook) :this()
        {
            extractor = "facebook";
            lstSubtitles.Visible = false;
            lbl_Subtitles.Visible = false;

            this.facebook = facebook;
            this.Text = facebook.title.Trim();
            picThumbnail.Visible = false;
            lblTitle.Text = facebook.title;
            txtDescription.Visible = false;
            lblDuration.Visible = false;
            foreach (Classes.Facebook.Format format in facebook.formats)
            {
                ListViewItem lstViewItm = new ListViewItem();
                lstViewItm.Text = format.format_note;
                lstViewItm.Tag = format.format_id;
                lstViewItm.SubItems.Add(format.ext);
                lstViewItm.SubItems.Add("N/A");
                lstViewItm.SubItems.Add(format.url);
                lstFormats.Items.Add(lstViewItm);
            }

        }

        public frmVideoInfo(Twitter twitter) :this()
        {
            extractor = "twitter";
            lstSubtitles.Visible = false;
            lbl_Subtitles.Visible = false;

            this.twitter = twitter;
            this.Text = twitter.title.Trim();
            if (!string.IsNullOrEmpty(twitter.thumbnail))
            {
                picThumbnail.Load(twitter.thumbnail.Trim());
            }
            else
            {
                picThumbnail.Visible = false;
            }
            lblTitle.Text = twitter.title.Trim();
            txtDescription.Text = twitter.description.Trim();
            lblDuration.Text = twitter.stringDuration;
            foreach (Classes.Twitter.Format format in twitter.formats)
            {
                ListViewItem lstViewItm = new ListViewItem();
                lstViewItm.Text = format.format;
                lstViewItm.Tag = format.format_id;
                lstViewItm.SubItems.Add(format.ext);
                lstViewItm.SubItems.Add("N/A");
                lstViewItm.SubItems.Add(format.url);
                lstFormats.Items.Add(lstViewItm);
            }
        }
        #endregion

        private void frmVideoInfo_Load(object sender, EventArgs e)
        {
            try
            {
                this.StyleManager = metroStyleManager;
            }
            catch (Exception) { }

            ToolTip1.SetToolTip(lblTitle, lblTitle.Text);
            this.Text = lblTitle.Text;
        }

        private void MenuCopyLink_Click(object sender, EventArgs e)
        {

            if (lstFormats.SelectedItems.Count > 0)
            {
                //Set URL to clipboard
                try
                {
                    Clipboard.Clear();
                    Clipboard.SetText(lstFormats.SelectedItems[0].SubItems[3].Text.Trim());
                }
                catch { }
            }

        }

        private void MenuOpenIDM_Click(object sender, EventArgs e)
        {
            if (lstFormats.SelectedItems.Count > 0)
            {

                IDM.result result = IDM.OpenIDM(lstFormats.SelectedItems[0].SubItems[3].Text.Trim());
                if (result == IDM.result.NotFound)
                {
                    //IDM Not Installed
                    MessageBox.Show("Can't find Internet Donwload Manager, 'IDM.exe' file",
                        "Can't find Internet Donwload Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if(result == IDM.result.Error)
                {
                    MessageBox.Show("Unknown error has occurred while execute process to open IDM",
                     "Unknown error has occurred", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void SubtitleCopyLink_Click(object sender, EventArgs e)
        {
            if (lstSubtitles.SelectedItems.Count > 0)
            {
                try
                {
                    //Set URL to clipboard
                    Clipboard.Clear();
                    Clipboard.SetText(lstSubtitles.SelectedItems[0].SubItems[2].Text.Trim());
                }
                catch { }
            }
        }

        private void SubtitleIDM_Click(object sender, EventArgs e)
        {
            if (lstSubtitles.SelectedItems.Count > 0)
            {
                //Get IDM path
                string idmPath = null;
                idmPath = Registry.GetValue(@"HKEY_CURRENT_USER\Software\DownloadManager", "ExePath", null).ToString();
                if (idmPath == null)
                {
                    //IDM Not Installed
                    MessageBox.Show("Can't find Internet Donwload Manager, 'IDM.exe' file"
                       , "Can't find Internet Donwload Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                else
                {
                    //Sent URL to IDM
                    try
                    {
                        Process procExtractor = new Process();
                        procExtractor.StartInfo.FileName = idmPath;
                        procExtractor.StartInfo.Arguments = @"/d " + lstSubtitles.SelectedItems[0].SubItems[2].Text.Trim();
                        procExtractor.StartInfo.UseShellExecute = false;
                        procExtractor.StartInfo.CreateNoWindow = true;
                        procExtractor.Start();
                    }
                    catch
                    {
                        MessageBox.Show("Unknown error has occurred while execute process to open IDM"
                      , "Unknown error has occurred", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            //Add Download Info
          
            if(extractor == "youtube")
            {
                foreach (ListViewItem item in lstFormats.CheckedItems)
                {
                    Download downloadInfo = new Download(video.id.Trim(), video.title.Trim(), item.Tag.ToString().Trim(),
                        item.SubItems[3].Text.Trim(), item.SubItems[1].Text.Trim(),
                        item.SubItems[2].Text.Trim(), "N/A", "0.0%", extractor);

                    Download.AddDownloadInfo(downloadInfo);
                }

                foreach (ListViewItem item in lstSubtitles.CheckedItems)
                {

                    Download downloadInfo = new Download(video.id.Trim(), video.id.Trim() + "." + item.Text.Trim(), item.Text.Trim(),
                        item.SubItems[2].Text.Trim(), item.SubItems[1].Text.Trim(), "N/A", "N/A", "0.0%", "youtube:subtitle");
                    
                   Download.AddDownloadInfo(downloadInfo);
                }
            }
            else if(extractor == "soundcloud")
            {
                foreach (ListViewItem item in lstFormats.CheckedItems)
                {
                    Download downloadInfo = new Download(soundCloud.id.Trim(), soundCloud.title.Trim(), item.Tag.ToString().Trim(),
                        item.SubItems[3].Text.Trim(), item.SubItems[1].Text.Trim(),
                        "N/A", "N/A", "0.0%", extractor);

                    Download.AddDownloadInfo(downloadInfo);
                }
            }
            else if (extractor == "facebook")
            {
                foreach (ListViewItem item in lstFormats.CheckedItems)
                {
                    Download downloadInfo = new Download(facebook.id.Trim(), facebook.title.Trim(), item.Tag.ToString().Trim(),
                        item.SubItems[3].Text.Trim(), item.SubItems[1].Text.Trim(),
                        item.SubItems[2].Text.Trim(), "N/A", "0.0%", extractor);
                    Download.AddDownloadInfo(downloadInfo);
                }
            }
            else if (extractor == "twitter")
            {
                foreach (ListViewItem item in lstFormats.CheckedItems)
                {
                    Download downloadInfo = new Download(twitter.id.Trim(), twitter.title.Trim(), item.Tag.ToString().Trim(),
                        item.SubItems[3].Text.Trim(), item.SubItems[1].Text.Trim(),
                        item.SubItems[2].Text.Trim(), "N/A", "0.0%", extractor);
                    Download.AddDownloadInfo(downloadInfo);
                }
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

     
    }
}
