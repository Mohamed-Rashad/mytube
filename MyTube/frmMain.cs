﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using MyTube.Classes;
using System.Collections.Generic;
using Newtonsoft.Json;
using MyTube.Classes.YouTube;
using System.Runtime.InteropServices;
using System.Xml;
using MyTube.Classes.SoundCloud;
using MyTube.Classes.Facebook;
using MyTube.Classes.Twitter;
using MyTube.Classes.Downloader.Classes;
using System.Text.RegularExpressions;
using System.Threading;
using System.Net;

namespace MyTube
{
    
    public partial class frmMain : MetroFramework.Forms.MetroForm
    {

        #region Declarations
        string downloadPath = Application.StartupPath + @"\Downloads";
        MediaPlayer.MediaPlayer mediaPlayer = new MediaPlayer.MediaPlayer();

        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        private static extern int SetWindowTheme(IntPtr hwnd, string pszSubAppName, string pszSubIdList);

        bool processIsRunning = false;
        Dictionary<Download, HttpDownloader> downloaders = new Dictionary<Download, HttpDownloader>();

        #endregion

        #region Constractors
        public frmMain()
        {
            InitializeComponent();
            try
            {
                SetWindowTheme(this.lstView.Handle, "Explorer", null);
                SetWindowTheme(this.lstView.Handle, "Explorer", null);
            }
            catch { }
        }
        #endregion

        #region General
        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                //init media player
                this.StyleManager = metroStyleManager;
                mediaPlayer.metroStyleManager = metroStyleManager;
                mediaPlayer.vlcLibDirectory = new DirectoryInfo(Application.StartupPath + @"\vlc");
                mediaPlayer.snapShotLocation = Application.StartupPath;
                mediaPlayer.parent = tabWatch;
                mediaPlayer.Init();
            }
            catch (Exception) { }

            RefreshDownloadLst();
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            Thread thread = new Thread(CheckUpdates);
            thread.Priority = ThreadPriority.Lowest;
            thread.Start();
        }

        private void txtUrl_ButtonClick(object sender, EventArgs e)
        {
            if (!IsConnected())
            {
                MessageBox.Show("Your computer is disconnected from the Internet!", "No internet connection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (processIsRunning)
            {
                Process[] pName = Process.GetProcessesByName("youtube-dl");
                foreach (Process process in pName)
                {
                    try
                    {
                        process.Kill();
                    }
                    catch { }
                }
                progrssBar.Visible = false;
                txtUrl.CustomButton.Image = MyTube.Properties.Resources.download_black;
                processIsRunning = false;
                return;
            }

            if (!string.IsNullOrEmpty(txtUrl.Text.Trim()))
            {
                progrssBar.Visible = true;
                txtUrl.CustomButton.Image = MyTube.Properties.Resources.close;
                GetVideoInfo(txtUrl.Text);
            }
        }

        private void menuDownload_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lstView.SelectedItems)
            {
                if (item.Tag != null)
                    DownloadFile((Download)item.Tag);
            }
        }

        private void menuStop_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lstView.SelectedItems)
            {
                if (item.Tag != null)
                {
                    if(downloaders.ContainsKey((Download)item.Tag))
                        downloaders[(Download)item.Tag].Cancel();
                }
            }
        }

        private void menuDelete_Click(object sender, EventArgs e)
        {
            bool userPermission = false;
            bool deleteFiles = false;
            foreach (ListViewItem item in lstView.SelectedItems)
            {
                if (item.Tag.GetType() == typeof(Download))
                {
                    //get download info from item tag.
                    Download download = (Download)item.Tag;
                    //remove download node from xml.
                    Download.RemoveDownloadInfo(download);

                    if(download.status != "N/A")
                    {
                        //get file info
                        FileInfo file = new FileInfo(FilterFileName(download.title.Trim() + " " +
                            download.format + "." + download.ext));
 
                        if (file.Exists)
                        {
                            if (!userPermission)
                            {
                                if (MessageBox.Show("Do you want to delete downloaded file(s) ?",
                                "Delete file(s)", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    deleteFiles = true;
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch (Exception)
                                    { }
                                }
                            }
                            else if(deleteFiles)
                            {
                                try
                                {
                                    file.Delete();
                                }
                                catch (Exception)
                                { }
                            }
                        }
                    }
                }

                lstView.Items.Remove(item);
            }
        }

        private void menuWatch_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lstView.SelectedItems)
            {
                if (item.Tag != null)
                {
                    Download download = (Download)item.Tag;
                    if (download.extractor.ToLower().Contains("subtitle"))
                    {
                        DownloadFile(download, false);
                    }
                    else
                    {
                        DownloadFile(download, true);
                    }
                    break;
                }
            }
        }

        private void lnkLicense_Click(object sender, EventArgs e)
        {
            Process.Start(@"http://www.gnu.org/licenses/gpl.html");
        }

        private void lnkSupport_Click(object sender, EventArgs e)
        {
            Process.Start(@"http://support.smartpcsoft.com");
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            foreach (HttpDownloader downloader in downloaders.Values)
            {
                downloader.Cancel();
            }
            Thread.Sleep(100);
            SaveDownloadLst();
        }

        private void lstView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            List<string> selectedfiles = new List<string>();


            foreach (ListViewItem item in lstView.SelectedItems)
            {
                if (item.Tag != null)
                {
                    Download download = (Download)item.Tag;
                    if (download.status == "Completed")
                    {
                        selectedfiles.Add(FilterFileName(download.title.Trim() + " " +
                            download.format + "." + download.ext));
                    }
                }
            }

            if (selectedfiles.Count > 0)
            {
                List<string> dropedFiles = new List<string>();

                foreach (string file in selectedfiles)
                {
                    if (new FileInfo(file).Exists)
                    {
                        dropedFiles.Add(file);
                    }
                }

                if (dropedFiles.Count <= 0)
                    return;

                DataObject data = new DataObject(DataFormats.FileDrop, dropedFiles.ToArray());
                lstView.DoDragDrop(data, DragDropEffects.Move);
            }

        }
        #endregion

        #region Methods
        public void GetVideoInfo(string videoURL,bool checkUpdates = false)
        {
            if (string.IsNullOrEmpty(videoURL) && checkUpdates == false)
            {
                progrssBar.Visible = false;
                txtUrl.CustomButton.Image = Properties.Resources.download_black;
                processIsRunning = false;
                return;
            }
           
            if (!File.Exists(Application.StartupPath + "\\youtube-dl.exe"))
            {
                progrssBar.Visible = false;
                txtUrl.CustomButton.Image = Properties.Resources.download_black;
                processIsRunning = false;
                MessageBox.Show("Missing file(s), Please re-install MyTube.", 
                    "Missing file(s)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                //Execute Process
                Process procExtractor = new Process();
                procExtractor.StartInfo.FileName = Application.StartupPath + "\\youtube-dl.exe";

                if (checkUpdates)
                {
                    procExtractor.StartInfo.Arguments = @"-U";
                    procExtractor.StartInfo.CreateNoWindow = false;
                    procExtractor.StartInfo.RedirectStandardOutput = false;
                }
                else
                {
                    procExtractor.StartInfo.Arguments = @"""" + videoURL + @"""" + " -J --write-sub -i --no-warnings";
                    procExtractor.StartInfo.CreateNoWindow = true;
                    procExtractor.OutputDataReceived += proc_OutputDataReceived;
                    procExtractor.StartInfo.RedirectStandardOutput = true;
                }

                procExtractor.StartInfo.UseShellExecute = false;
                procExtractor.Start();

                if (!checkUpdates)
                {
                    processIsRunning = true;
                    procExtractor.BeginOutputReadLine();
                }
                    
            }
            catch
            {
                MessageBox.Show("Unknown error has occurred while execute process."
                        , "Unknown error has occurred", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void proc_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null)
                return;

            if (e.Data.Equals("null") || string.IsNullOrWhiteSpace(e.Data))
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    progrssBar.Visible = false;
                    txtUrl.CustomButton.Image = MyTube.Properties.Resources.download_black;
                    processIsRunning = false;
                    MessageBox.Show("Unknown error has occurred while execute process."
                        , "Unknown error has occurred", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }));
                return;
            }

            string videoExtractor = JsonConvert.DeserializeObject<videoExtractor>(e.Data).extractor.ToLower().Trim();

            Invoke(new MethodInvoker(delegate ()
            {
                progrssBar.Visible = false;
                txtUrl.CustomButton.Image = MyTube.Properties.Resources.download_black;
                processIsRunning = false;

                Form frmVideoInfo = null;

                if (videoExtractor == "youtube")
                {
                    YouTube video = JsonConvert.DeserializeObject<YouTube>(e.Data);
                    frmVideoInfo = new frmVideoInfo(video);

                }
                else if (videoExtractor == "youtube:playlist")
                {
                    YouTubePlayList youtubePlayList = JsonConvert.DeserializeObject<YouTubePlayList>(e.Data);
                    Form frmLstInfo = new frmLstInfo(youtubePlayList);
                    frmLstInfo.ShowDialog(this);
                    if (frmLstInfo.DialogResult == DialogResult.OK)
                    {
                        RefreshDownloadLst();
                    }
                    return;
                }
                else if (videoExtractor == "soundcloud")
                {
                    SoundCloud soundCloud = JsonConvert.DeserializeObject<SoundCloud>(e.Data);
                    frmVideoInfo = new frmVideoInfo(soundCloud);
                }
                else if (videoExtractor == "facebook")
                {
                    Facebook facebook = JsonConvert.DeserializeObject<Facebook>(e.Data);
                    frmVideoInfo = new frmVideoInfo(facebook);
                }
                else if (videoExtractor.Contains("twitter"))
                {
                    Twitter twitter = JsonConvert.DeserializeObject<Twitter>(e.Data);
                    frmVideoInfo = new frmVideoInfo(twitter);
                }

                if (frmVideoInfo != null)
                {
                    frmVideoInfo.ShowDialog(this);
                    if (frmVideoInfo.DialogResult == DialogResult.OK)
                    {
                        RefreshDownloadLst();
                    }
                }

            }));
        }

        private void RefreshDownloadLst()
        {
            List<Download> downloads = new List<Download>(); //List of downloads

            if (File.Exists("downloads.xml"))
            {
                XmlDocument document = new XmlDocument();
                document.Load("downloads.xml"); // Load document

                //Deserialize XML file
                foreach (XmlElement element in document.GetElementsByTagName("file"))
                {
                    //file nodes
                    XmlNodeList idNode = element.GetElementsByTagName("id");
                    XmlNodeList titleNode = element.GetElementsByTagName("title");
                    XmlNodeList urlNode = element.GetElementsByTagName("url");
                    XmlNodeList formatNode = element.GetElementsByTagName("format");
                    XmlNodeList extNode = element.GetElementsByTagName("ext");
                    XmlNodeList sizeNode = element.GetElementsByTagName("size");
                    XmlNodeList statusNode = element.GetElementsByTagName("status");
                    XmlNodeList progressNode = element.GetElementsByTagName("progress");
                    XmlNodeList extractorNode = element.GetElementsByTagName("extractor");

                    //check if nodes is exists
                    if (idNode.Count > 0 && titleNode.Count > 0 && urlNode.Count > 0
                        && formatNode.Count > 0 && extNode.Count > 0 && sizeNode.Count > 0
                        && statusNode.Count > 0 && progressNode.Count > 0 && extractorNode.Count > 0)
                    {
                        Download downloadItm = new Download();
                        downloadItm.id = idNode[0].InnerText.Trim();
                        downloadItm.title = titleNode[0].InnerText.Trim();
                        downloadItm.url = urlNode[0].InnerText.Trim();
                        downloadItm.format = formatNode[0].InnerText.Trim();
                        downloadItm.ext = extNode[0].InnerText.Trim();
                        downloadItm.fileSize = sizeNode[0].InnerText.Trim();
                        downloadItm.status = statusNode[0].InnerText.Trim();
                        downloadItm.progress = progressNode[0].InnerText.Trim();
                        downloadItm.extractor = extractorNode[0].InnerText.Trim();
                        //add download item to the download list
                        downloads.Add(downloadItm);
                    }
                }
            }

            //Add downloads list to the listview        
            foreach (Download download in downloads)
            {
                bool isExists = false;
                foreach (ListViewItem item in lstView.Items)
                {
                    if (item.Tag != null)
                    {
                        if (item.Tag.GetType() == typeof(Download))
                        {
                            Download downloadInfo = (Download)item.Tag;
                            if (download.id == downloadInfo.id && download.format == downloadInfo.format &&
                                download.extractor == downloadInfo.extractor)
                            {
                                isExists = true;
                                break;
                            }
                        }
                    }
                }

                if (isExists)
                    continue;

                ListViewItem listViewItem = new ListViewItem();
                listViewItem.Text = download.title;
                //add download object to the item tag
                listViewItem.Tag = download;
                listViewItem.SubItems.Add(download.fileSize);
                listViewItem.SubItems.Add(download.status);
                listViewItem.SubItems.Add(download.progress);
                listViewItem.SubItems.Add("N/A");
                if (download.extractor.ToLower().Trim() == "youtube:subtitle")
                {
                    listViewItem.ImageIndex = 1;
                }
                else if (download.extractor.ToLower().Trim() == "soundcloud")
                {
                    listViewItem.ImageIndex = 2;
                }
                else if (download.extractor.ToLower().Trim() == "facebook")
                {
                    listViewItem.ImageIndex = 3;
                }
                else if (download.extractor.ToLower().Trim().Contains("twitter"))
                {
                    listViewItem.ImageIndex = 4;
                }
                else
                {
                    listViewItem.ImageIndex = 0;
                }

                lstView.Items.Add(listViewItem);
            }

        }

        private void SaveDownloadLst()
        {
            foreach (ListViewItem item in lstView.Items)
            {
                if (item.Tag != null)
                {
                    if (item.Tag.GetType() == typeof(Download))
                        Download.AddDownloadInfo((Download)item.Tag);
                }
            }
        }

        private bool IsConnected()
        {
            return System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
        }

        private void DownloadFile(Download file,bool watch=false)
        {
            if (!IsConnected())
            {
                MessageBox.Show("Your computer is disconnected from the Internet!", "No internet connection",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string filePath;

            if(!file.extractor.Contains("subtitle"))
            {
                filePath = FilterFileName(file.title.Trim() + " " + file.format + "." + file.ext);
            }
            else
            {
                filePath = FilterFileName(file.title.Trim() + "." + file.ext);
            }

            HttpDownloader downloader = new HttpDownloader(file.url.Trim(), filePath);

            if (file.status != "Completed" || !File.Exists(filePath))
            {
                if (downloaders.ContainsKey(file))
                {
                    downloader = downloaders[file];
                    downloaders[file].StartAsync();
                }
                else
                {
                    downloader.Id = file;
                    downloader.DownloadCancelled += Downloader_DownloadCancelled;
                    downloader.DownloadCompleted += Downloader_DownloadCompleted;
                    downloader.DownloadError += Downloader_DownloadError;
                    downloader.DownloadProgressChanged += Downloader_DownloadProgressChanged;

                    downloaders.Add(file, downloader);
                    downloader.StartAsync();
                }
            }

            if (watch == true)
            {
                tabControl.SelectedIndex = 1;
                tabWatch.Tag = file;

                string[] mediaOptions = new string[1];


                //search for subtitle
                if (file.extractor.ToLower().Contains("youtube"))
                {
                    foreach (ListViewItem item in lstView.Items)
                    {
                        if (item.Tag != null)
                        {
                            Download download = (Download)item.Tag;
                            if (download.extractor.ToLower().Contains("subtitle"))
                            {
                                if (file.id == download.id)
                                {
                                    mediaOptions[0] = ":sub-file=" + FilterFileName(download.id.Trim() + "." +
                                         download.format.Trim() + "." + download.ext.Trim());
                                    break;
                                }
                            }
                        }
                    }
                }

                mediaPlayer.SetMedia(new FileInfo(filePath), mediaOptions);

                if(file.status == "Completed" && File.Exists(filePath))
                {
                    mediaPlayer.downloaded = 100;
                    mediaPlayer.Play();
                }
                else
                {
                   if (downloader.State == Classes.Downloader.Enums.DownloadState.Downloading ||
                   downloader.State == Classes.Downloader.Enums.DownloadState.Started)
                    {
                        while (downloader.Progress < 1)
                        {
                            Application.DoEvents();
                        }
                        mediaPlayer.Play();
                    }
                    else if (downloader.State == Classes.Downloader.Enums.DownloadState.Completed)
                    {
                        mediaPlayer.downloaded = 100;
                        mediaPlayer.Play();
                    }
                } 
            }

        }

        private string FilterFileName(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return null;
            if (!Directory.Exists(downloadPath))
                Directory.CreateDirectory(downloadPath);
            string regexSearch = new string(Path.GetInvalidFileNameChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            fileName = r.Replace(fileName, "");
            return downloadPath + @"\" + fileName;
        }

        private void CheckUpdates()
        {
            if (IsConnected())
            {
                //check main version
                try
                {  
                    HttpWebRequest request = WebRequest.Create(@"http://smartpcsoft.com/info/my-tube/version.php") as HttpWebRequest;
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    string result = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8).ReadToEnd();
                    Version serverVersion = new Version(result);
                    Version localVersion = new Version(Application.ProductVersion);
                    if (serverVersion > localVersion)
                    {
                        DialogResult msg = DialogResult.Cancel;
                        this.Invoke(new Action(() => {
                            msg = MessageBox.Show(this, "New updates are available for MyTube." +
                                Environment.NewLine + "Do you want to download the new updates?",
                                "New Updates", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, 
                                MessageBoxDefaultButton.Button1);
                        }));
                        if (msg == DialogResult.OK)
                        {
                            Process.Start("http://smartpcsoft.com/mytube.html");
                        }
                    }
                }
                catch { }
                //check youtube-dl version
                try
                {
                    HttpWebRequest request = WebRequest.Create(@"https://rg3.github.io/youtube-dl/update/LATEST_VERSION") as HttpWebRequest;
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    string result = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8).ReadToEnd();
                    Version serverVersion = new Version(result);
                    Version localVersion = new 
                        Version(FileVersionInfo.GetVersionInfo(Application.StartupPath +
                        @"\youtube-dl.exe").FileVersion);

                    if (serverVersion > localVersion)
                    {
                        DialogResult msg = DialogResult.Cancel;
                        this.Invoke(new Action(() => {
                            msg = MessageBox.Show(this, "New updates are available for MyTube." +
                                Environment.NewLine + "Do you want to download the new updates?", 
                                "New Updates", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, 
                                MessageBoxDefaultButton.Button1);
                        }));
                        if (msg == DialogResult.OK)
                        {
                            GetVideoInfo(null, true);
                        }
                    }
                }
                catch { }
            }
        }
        #endregion

        #region Events
        private void Downloader_DownloadProgressChanged(object sender, Classes.Downloader.Classes.DownloadProgressChangedEventArgs e)
        {
            HttpDownloader downloader = (HttpDownloader)sender;
            foreach (ListViewItem item in lstView.Items)
            {
                if (item.Tag != null)
                {
                    if (item.Tag == downloader.Id)
                    {
                        if (item.Tag == tabWatch.Tag)
                            mediaPlayer.downloaded = e.Progress;

                        if (item.SubItems[1].Text != SizeUnitConverter.ConvertBestScaledSize(downloader.ContentSize))
                        {
                            item.SubItems[1].Text = SizeUnitConverter.ConvertBestScaledSize(downloader.ContentSize);
                            downloader.Id.fileSize = SizeUnitConverter.ConvertBestScaledSize(downloader.ContentSize);
                        }
                        item.SubItems[2].Text = downloader.State.ToString();
                        item.SubItems[3].Text = e.Progress.ToString() + @"%";
                        item.SubItems[4].Text = SizeUnitConverter.ConvertBestScaledSize(e.Speed);
                        downloader.Id.status = downloader.State.ToString();
                        downloader.Id.progress = e.Progress.ToString() + @"%";
                        item.Tag = downloader.Id;
                    }
                }
            }
        }

        private void Downloader_DownloadCompleted(object sender, EventArgs e)
        {
            HttpDownloader downloader = (HttpDownloader)sender;
            foreach (ListViewItem item in lstView.Items)
            {
                if (item.Tag != null)
                {
                    if (item.Tag == downloader.Id)
                    {
                        downloader.Id.status = "Completed";
                        item.Tag = downloader.Id;
                        item.SubItems[2].Text = downloader.State.ToString();
                    }
                }
            }
        }

        private void Downloader_DownloadCancelled(object sender, EventArgs e)
        {
            HttpDownloader downloader = (HttpDownloader)sender;
            foreach (ListViewItem item in lstView.Items)
            {
                if (item.Tag != null)
                {
                    if (item.Tag == downloader.Id)
                    {
                        item.SubItems[2].Text = downloader.State.ToString();
                    }
                }
            }
        }

        private void Downloader_DownloadError(object sender, EventArgs e)
        {
            MessageBox.Show("An error occurred during download file, Please update the file URL!",
                "Unkown Error",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
        }

        #endregion

    }
}
